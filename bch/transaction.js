'use strict';
const bch = require('bitcoincashjs');
const fs = require('fs');
const path = require('path');
const { Script, Address, PublicKey, PrivateKey, Transaction} = bch;

const request = require('request-promise-native')

const API = 'https://bch-chain.api.btc.com/v3';

const fetchUnspents = (address) =>
  request(`${API}/address/${address}/unspent`).then(JSON.parse).then(res => {
    return res.data.list;
  });

// const broadcastTx = (txRaw) =>
//   request.post(`${API}/tx/send`, {
//     json: true,
//     body: {
//       rawtx: txRaw,
//     },
//   })



async function test() {
  try {
    const address = '1HuzESBszka4zi4G1zVjBmARvAmZb1Z7SP';
    const unspents = await fetchUnspents(address);
    console.log(unspents);
    const feeValue      = 668
    const totalUnspent  = unspents.reduce((summ, { value }) => summ + value, 0)
    const skipValue     = totalUnspent - feeValue

    const utxoSpend = [];

    var privateKeys = [
      new PrivateKey('7066d222311621c190ff1a6fe892c5f4110f5f3cdf678dd334b38e38984193ae'),
    ];

    unspents.forEach(tx => {
      utxoSpend.push({
        "txId" : tx.tx_hash,
        "outputIndex" : tx.tx_output_n,
        "address" : address,
        script: new Script(new Address(address)).toHex(),
        "satoshis" : tx.value,
      });
    });

    const tx = new Transaction();
    tx.from(utxoSpend);
    tx.to('39wfkdLRdKQuKdais157JAPh75aUjS5MtX', skipValue)
    .fee(feeValue)
    .sign(privateKeys)

    console.log(tx.serialize());
    return tx;
  } catch (err) {
    console.log(err.stack);
  }
}


test();

'use strict';
const bch = require('bitcoincashjs');
const fs = require('fs');
const path = require('path');
const keysFile = path.join(__dirname, 'key.txt');
const addressFle = path.join(__dirname, 'address.txt');
const Address = bch.Address;
const fromString = Address.fromString;
const BitpayFormat = Address.BitpayFormat;
const CashAddrFormat = Address.CashAddrFormat;

var bchaddr = require('bchaddrjs');
var toLegacyAddress = bchaddr.toLegacyAddress;
var toBitpayAddress = bchaddr.toBitpayAddress;
var toCashAddress = bchaddr.toCashAddress;
// const cashaddr = fromString('bitcoincash:qr0q67nsn66cf3klfufttr0vuswh3w5nt5jqpp20t9',
//                   'livenet', 'pubkeyhash', CashAddrFormat);
// console.log(cashaddr);

const cashaddr = require('cashaddrjs');
const address = 'bitcoincash:qqvutlqyd8d89jwd8vwk9tuf4cn0cshwy5anf80cfz';
const xx = toLegacyAddress(address);
console.log(xx);

let count = 0;
while (true) {
  count++;
  const privateKey = new bch.PrivateKey();
  const key = privateKey.toString();
  const address = privateKey.toAddress().toString();
  fs.appendFileSync(keysFile, `${address}:${key}\n`);
  fs.appendFileSync(addressFle, address.toString() + '\n');
  return
  if (count % 10000 === 0) {
    console.log(count);
  }
}

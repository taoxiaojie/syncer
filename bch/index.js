'use strict';
const urllib = require('urllib');
const bchaddr = require('bchaddrjs');
const BaseSyncer = require('../lib/bit_syncer');
const debug = require('debug')('bch');
const Client = require('bitcoin-core');
const block_port = process.env.block_port || 8332;

module.exports = class Syncer extends BaseSyncer {
  constructor(options) {
    super(options);
    this.baseUrl = `http://${this.host}:${block_port}/rest/`;
    this.client = new Client({
      host: this.host,
      username: 'bdeals',
      password: 'bdeals_bitcoin',
      port: block_port
    });
    this.db = new this.DataBase(this.assetsName);
  }

  get assetsName() {
    return 'bch';
  }

  get assets_type() {
    return 3;
  }

  get confirmNumber() {
    return parseInt(process.env.confirmNumber) || 2;
  }

  get minAmount() {
    return 0.0025;
  }

  getAddress(address, type) {
    if (address.indexOf('bitcoincash:') === 0) {
      address = bchaddr.toLegacyAddress(address);
    }
    return address.toString();
  }
}

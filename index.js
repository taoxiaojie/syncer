'use strict';
const type = process.env.type;
const BtcWatcher = require('./watchers/btc');
const BtcHandler = require('./handlers/btc');
const ETHHandler = require('./handlers/eth');
const ETHWatcher = require('./watchers/eth');
const USDTWatcher = require('./watchers/usdt');
const USDTHandler = require('./handlers').usdt;
const BTCCollector = require('./collector/btc');
const BCHWatcher = require('./watchers/bch');
const BCHHandler = require('./handlers/bch');

const xpubkey = 'xpub6CyxToyiN9fSxr8T3f54Mxuwxr8o5anGAqDmxR9eaM9N4q1sxBh2UWxtPuqDhcSbPTpjBsvnuJiM3EtUvModZq1tHxg6oQWYoUCVL5S6SSD';
const watcher = new BtcWatcher({
  host: '192.168.0.2',
  block_port: '8332',
  start_block: 543156,
});

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.stack);
});

// const ethWatcher = new ETHWatcher({
//   start_block: 6359947,
// });
//
// new ETHHandler({
//   watcher: ethWatcher,
// });

const usdtWatcher = new USDTWatcher({
  host: '192.168.0.2',
  block_port: 32770,
  start_block: 543156,
});

new USDTHandler({
  watcher: usdtWatcher,
});

new BCHHandler({
  watcher: new BCHWatcher({
    host: '192.168.0.5',
    block_port: '32773',
    start_block: 543156,
  })
});

new BTCCollector({
  targetAddress: '1BfthQHkEQL2iLNuoD8uuUmxrNacDGfgfg',
  secret: '80fdb2a0ff09e03de3723f6cc08f5651',
  xpubkey,
  handler: new BtcHandler({
    watcher,
  }),
});

return ;

if (type === 'btc') {
  require('./btc/index');
} else if (type === 'eth') {
  require('./eth/index');
} else if (type === 'bch') {
  require('./bch/index');
} else if (type === 'ltc') {
  require('./ltc/index');
} else if (type === 'usdt') {
  require('./usdt/index');
} else {
  console.error('type is Invalid');
  process.exit();
}

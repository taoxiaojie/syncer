'use strict';
const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const router = new Router();

router.get('/', (ctx, next) => {
  // ctx.router available
  console.log(ctx);
  ctx.body = '123';
});

// 处理交易
router.get('/transactions/process', (ctx, next) => {
  const query = ctx.query;
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(9090);

'use strict';
const Web3 = require('web3');
const Base = require('./base');
const BigNumber = require('bignumber.js');
const ERC20 = require('../lib/erc20/erc20.json');
const tokenMap = require('../lib/erc20/tokens');

module.exports = class ETHWatcher extends Base {
  constructor(options={}) {
    super(options);
    this.urls = [
      //'https://api.myetherapi.com/eth',
      // 'https://mainnet.infura.io/qexcS4J1PiCKn9WlivBs',
      'http://47.75.55.45:8545',
    ];

    this.index = 0;
    if (options.url) {
      this.urls = this.urls.concat(options.url);
    }
    this.providers = [];
  }

  get tokenName() {
    return 'eth';
  }

  async init() {
    const urls = this.urls;
    for (let url of urls) {
      await this.addProvider(url);
    }
    await super.init();
  }

  async getBlockHashByHeight(height) {
  }

  async getTx(txid) {
  }

  get utxo() {
    return false;
  }

  get minAmount() {
    return super.minAmount || 0.01;
  }

  get assetsName() {
    return 'eth';
  }

  get assets_type() {
    return 2;
  }

  get confirmNumber() {
    return parseInt(process.env.confirmNumber) || 15;
  }

  get minAmount() {
    return 0.01;
  }

  getTransactionByHash(txid) {
    const provider = this.getProvider();
    if (provider) {
      return new Promise(resolve => {
        provider.eth.getTransaction(txid, (err, data) => {
          if (err) {
            console.log(err);
          }
          if (data) {
            resolve(Object.assign(data,{
              hash: data.blockHash,
              height: data.blockNumber,
            }));
          } else {
            resolve();
          }
        });
      });
    } else {
      console.log('no provider');
      return Promise.resolve();
    }
  }

  // 同步未确认的区块
  async syncPenddingTransactions() {
    while (true) {
      await this.sleep('3s');
      const provider = this.getProvider();
      if (!provider) {
        continue;
      }
      const block = await this._getBlock(provider, 'pending');
      const transactions = block && block.transactions
      if (transactions && transactions.length) {
        for (let tx of transactions) {
          this.handleTx(tx);
        }
      }
    }
  }

  async getLatestBlock() {
    const provider = this.getProvider();
    if (!provider) {
      return;
    }
    const block = await this._getBlock(provider, 'latest');
    // console.log(block);
    // block.transactions = await this.getTransactionsByBlock(block);
    return block;
  }

  async addProvider(url) {
    const checkHeight = '5684588';
    const checkHash = '0x2642a15f1891ce294a412b549dfebaeac243cac7eb9e2050de6ebd890b22c8ee';
    const provider = new Web3(url);
    const block = await this._getBlock(provider, checkHeight, false);
    this.debug(`added provider ${url}, block is `, block);
    if (block && block.hash === checkHash) {
      this.providers.push(provider);
      const contracts = {};
      for (let address in tokenMap) {
        const token = tokenMap[address];
        contracts[token.address] = new provider.eth.Contract(ERC20, address);
      }
      provider.contracts = contracts;
    }
  }

  async getTransactionsByBlock(block) {
    const transactions = block.transactions;
    const txs = [];
    for (let txid of transactions) {
      const tx = await this.getTransactionByHash(txid);
      txs.push(tx);
    }
    return txs;
  }

  async handleTx(transaction) {
    const {blockHash, blockNumber, to, value, hash, from} = transaction;
    if (!to) {
      return;
    }

    const address = to.toLowerCase();
    let amount = this.formatAmount(Web3.utils.fromWei(value));

    const tx = {
      block_height: blockNumber || 0,
      block_hash: blockHash || '',
      txid: hash,
      value: amount,
      address,
      token: 'ETH',
    };

    this.emit('erc20_transaction', tx);

    // erc20合约转账
    const erc20Token = tokenMap[address]
    if (blockNumber && erc20Token) {
      const events = await this.queryErc20Transaction(address, blockNumber);
      if (events && events.length) {
        for (let event of events) {
          const tokenTx = event.returnValues;
          const {from, to, tokens} = tokenTx;
          const amount = BigNumber(tokens).shiftedBy(-erc20Token.decimals).toString();
          this.emit('erc20_transaction', {
            ...tx,
            address: to.toLowerCase(),
            value: amount,
            token: erc20Token.name,
          });
        }
      }
    }
  }

  async queryErc20Transaction(address, blockNumber) {
    const provider = this.getProvider();
    if (!provider) {
      return;
    }
    const contract = provider.contracts[address];
    const events = await contract.getPastEvents('Transfer', {
      fromBlock: blockNumber,
      toBlock: blockNumber,
    });

    if (events && events.length) {
      return events;
    }
  }

  handlePendingTx(tx) {

  }

  getProvider() {
    const providers = this.providers;
    if (!providers.length) {
      return;
    }
    const index = (this.index++)%providers.length;
    return providers[index];
  }

  async getBlock(lastBlock) {
    const provider = this.getProvider();
    if (!provider) {
      return;
    }
    const nextBlock = lastBlock.height + 1;
    let block = await this._getBlock(provider, nextBlock);
    return block;
  }

  async getBlockByHeight(height) {
    return await this.getBlock({
      height: height - 1,
    });
  }

  _getBlock(provider, blockHashOrBlockNumber, returnTransactionObjects = true) {
    // 3分钟还没同步到区块就自动退出
    const startTime = Date.now();
    const timer = setTimeout(() => {
      process.exit(0);
    }, 3 * 60 * 1000);

    return new Promise((resolve, reject) => {
      clearTimeout(timer);
      provider.eth.getBlock(blockHashOrBlockNumber, returnTransactionObjects, (err, data) => {
        if (err) {
          console.log(err);
          return resolve();
        }
        resolve(data);
      });
    });
  }
}

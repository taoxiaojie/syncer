// 处理比特币系列的交易记录
'use strict'
const Base = require('./base');
const urllib = require('urllib');

module.exports = class SatoshisWatcher extends Base {
  constructor(options) {
    super(options);
    this.baseUrl = `http://${options.host}:${options.block_port}/rest/`;
  }

  // 获取内存中等待打包的交易
  async syncPenddingTransactions() {
    const data = await this._request('/mempool/contents.json');
    for (let txid in data) {
      const tx = await this.getTx(txid);
      await this.sleep(100);
      this.handlePendingTx(tx);
    }
    await this.sleep('20s');
  }

  // 获取当前最新区块hash
  async getLatestBlock() {
    const info = await this._request('chaininfo.json');
    const bestblockhash = info.bestblockhash;
    return await this.getBlockByHash(bestblockhash);
  }

  async getBlock(lastBlock={}) {
    let hash = lastBlock.nextblockhash;
    const height = lastBlock.height;
    if (!hash) {
      hash = await this.getBlockHashByHeight(height + 1);
    }
    return await this.getBlockByHash(hash);
  }

  async getBlockByHeight(height) {
    const hash = await this.getBlockHashByHeight(height);
    return await this.getBlockByHash(hash);
  }

  async getTransactionByHash(txid) {
    const data = await this.getTx(txid);
    if (data) {
      return {
        ...data,
        hash: data.blockhash,
      };
    }
  }

  async getBlockByHash(hash) {
    if (!hash) {
      return;
    }
    const block = await this._request(`/block/${hash}.json`);
    return block;
  }

  async _request(api) {
    if (api.indexOf('/') === 0) {
      api = api.substring(1);
    }
    const url = this.baseUrl + api;
    const res = await urllib.request(url, {
      timeout: '10s',
      dataType: 'json',
    });
    return res.data;
  }

  async getBlockTransactions(block) {
    return block.tx;
  }

  getAddress(address, type) {
    return address;
  }
}

'use strict';
const urllib = require('urllib');
const debug = require('debug')('btc');
const OmniClient = require('../lib/omniClient.js').OmniClient;
const gather = require('co-gather');
const BtcWatcher = require('./btc');

module.exports = class USDTWatcher extends BtcWatcher {
  constructor(options={}) {
    options.addressName = 'btc';
    super(options);

    this.omniclient = new OmniClient({
      host: options.host,
      port: options.block_port,
      user: 'bdeals',
      pass: 'bdeals_bitcoin',
    });
  }

  get tokenName() {
    return 'usdt';
  }

  async beforeInit() {

  }

  async init() {
    super.init();
    let lastBlock = await this.getLastBlock();
    let latestBlock = this.latestBlock;
    if (!latestBlock) {
      latestBlock = await this.getLatestBlock();
    }

    if (lastBlock && lastBlock.height && latestBlock && latestBlock.height) {
      this.catchUp(lastBlock.height, latestBlock.height);
    }
  }

  // 同步未确认的区块
  async syncPenddingTransactions() {
    const list = await this.omniclient.omniListPendingTransactions();
    for (let tx of list) {
      const propertyid = parseInt(tx.propertyid);
      // pending 状态的交易没有 valid 字段
      if (propertyid === 31) {
        this.handlePendingTx({
          block_height: 0,
          block_hash: '',
          txid: tx.txid,
          address: tx.referenceaddress,
          value: tx.amount,
        });
      }
    }
  }

  handlePendingTx(tx) {
    this.publishPendingTx(tx);
  }

  async getLatestBlock() {
    const block = await super.getLatestBlock();
    return block;
  }

  handleTx(tx) {
    this.publishTx(tx);
  }

  async catchUp(from, last) {
    let page = 0;
    console.log(`start catch up from ${from} to ${last}`);
    do {
      try {
        const { data } = await urllib.request('https://api.omniexplorer.info/v1/transaction/general/' + page, {
          dataType: 'json',
        });

        for (const tx of (data.transactions || [])) {
          if (!(tx.block && tx.valid)) continue;
          if (from > tx.block) return; // 超出指定范围
          if (parseInt(tx.propertyid) !== 31) continue;
          this.handleTx({
            block_height: tx.block,
            block_hash: tx.blockhash,
            txid: tx.txid,
            address: tx.referenceaddress,
            value: tx.amount,
          });
        }
        page++;
      } catch (e) {
        page--;
        console.log(e);
      }
      await this.sleep(10000);
    } while (true);
  }

  // 根据交易id获取交易详细
  async getTx(txid) {
    try {
      return await this.omniclient.omniGetTransaction(txid);
    } catch (err) {
      const url = `https://api.omniexplorer.info/v1/transaction/tx/${txid}`;
      const res = await urllib.request(url, {
        dataType: 'json',
      });
      return res.data;
    }
  }

  async getBlock(lastBlock) {
    const height = lastBlock.height;
    return await this.getBlockByHeight(height + 1);
  }

  async getBlockTransactions(block) {
    const txs = await this.omniclient.omniListBlockTransactions(block.height);
    const transactions = [];
    for(let id of txs) {
      const tx = await this.getTx(id);
      if (parseInt(tx.propertyid) == 31 && tx.valid) {
        transactions.push({
          block_height: tx.block,
          block_hash: tx.blockhash,
          txid: tx.txid,
          address: tx.referenceaddress,
          value: tx.amount,
        });
      }
    }
    return transactions;
  }
}

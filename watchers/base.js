'use strict';

const sleep = require('ko-sleep');
const path = require('path');
const mkdirp = require('mkdirp');
const fs = require('fs');
const os = require('os');
const ms = require('ms');
const debug = require('debug');
const Base = require('sdk-base');
const LRU = require("lru-cache");
const homedir = require('../lib/homedir');
const NodeRSA = require('node-rsa');
const urllib = require('urllib');
const CryptoJS = require("crypto-js");
// const rsa = new NodeRSA(Buffer.from(process.env.privateKey, 'hex').toString());
const assert = require('assert');
const dataId = 'withdraw';
const group = 'DEFAULT_GROUP';

module.exports = class BaseWatcher extends Base {
  constructor(options) {
    super({
      initMethod: 'init',
    });
    this.options = options;
    this.debug = debug(this.tokenName || 'baseSyncer');
    const dir = path.join(homedir, `.${this.tokenName}.sync`);
    mkdirp.sync(dir);
    this.lastBlockFile = path.join(dir, 'lastblock');
    this.cache = LRU({
      max: 5000,
      maxAge: 1000 * 60,
    });
  }

  log(name, data) {
    const file = path.join(__dirname, '../logs', name);
    fs.appendFileSync(file, JSON.stringify(data, null, 2));
  }

  onBlock(block) {
    const formattedBlock = this.formatBlock(block);
    this.debug('fetched block', formattedBlock.height);
    const cache = this.cache;
    const height = formattedBlock.height;
    // 该区块已经处理过
    if (cache.get(height)) {
      return;
    }
    this.cache.set(height, 1);
    this.handleBlock(block);
  }

  async getBlockTransactions(block) {
    return block.transactions;
  }

  async handleBlock(block) {
    const formattedBlock = this.formatBlock(block);
    const transactions = await this.getBlockTransactions(block);
    if (!(transactions && transactions.length)) {
      return;
    }
    for (let tx of transactions) {
      this.handleTx({
        ...tx,
        block_hash: formattedBlock.hash,
        block_height: formattedBlock.height
      });
    }
  }

  handleTx(transaction) {
    this.publishTx(transaction);
  }

  // 获取上一次同步的区块
  async getLastBlock() {
    if (this.lastBlock) {
      return this.lastBlock;
    }

    if (this.options.start_block) {
      const blockHeight = parseInt(this.options.start_block) - 1;
      const block = await this.getBlockByHeight(blockHeight);
      if (block) {
        this.onBlock(block);
        return this.formatBlock(block);
      }
    }

    const lastBlockFile = this.lastBlockFile;

    if (fs.existsSync(lastBlockFile)) {
      const content = fs.readFileSync(lastBlockFile).toString();
      return JSON.parse(content);
    } else {
      const block = await this.getLatestBlock();
      if (block) {
        this.onBlock(block);
        return this.formatBlock(block);
      }
    }
  }

  formatAmount(amount) {
    amount = amount.toString();
    const index = amount.indexOf('.');
    if (index > -1) {
      amount = amount.substring(0, index + 9);
    }
    return amount;
  }

  updateLastBlock() {

  }

  // 同步区块
  async startSyncBlock() {
    while (true) {
      try {
        let time = Date.now();
        this.debug(`start sync block, last block is (%j)`, this.lastBlock);
        const latestBlock = this.latestBlock;
        // 已经是最新区块，不需要做同步了
        if (latestBlock && latestBlock.height === this.lastBlock.height) {
          await sleep('10s');
          continue;
        }
        const nextBlock = await this.getBlock(this.lastBlock);
        this.debug(`synced block ${(Date.now() - time)/1000}s`);
        if (nextBlock) {
          const block = this.formatBlock(nextBlock);
          this.onBlock(nextBlock);
          this.lastBlock = block;
          await sleep(100);
        } else {
          await sleep('10s');
        }
      } catch(err) {
        console.log(err);
        await sleep('10s');
      }
    }
  }

  async syncLatestBlock() {
    while (true) {
      try {
        const block = await this.getLatestBlock();
        if (block) {
          this.onBlock(block);
          this.publishNewBlock(block);
          this.latestBlock = this.formatBlock(block);
        }
      } catch (err) {
        console.log(err);
      }
      await this.wait();
    }
  }

  async beforeInit() {

  }

  async init() {
    await this.beforeInit();
    const lastBlock = await this.getLastBlock();
    if (lastBlock) {
      this.lastBlock = lastBlock;
    }
    this.syncLatestBlock();
    this.startSyncPenddingTransactions();
    this.startSyncBlock();
  }

  async startSyncPenddingTransactions() {
    while (true) {
      try {
        await this.syncPenddingTransactions();
      } catch (err) {
        console.log(err);
      }
    }
  }

  publishPendingTx(tx, raw) {
    this.emit('pending_transaction', tx, raw);
  }

  publishTx(tx, raw) {
    this.emit('transaction', tx, raw);
  }

  publishNewBlock(block) {
    this.emit('new_block', block);
  }

  async sleep(time) {
    await sleep(time);
  }

  async wait() {
    await sleep('10s');
  }

  formatBlock(block) {
    return {
      hash: block.hash,
      height: block.height || block.number,
      nextblockhash: block.nextblockhash,
      timestamp: block.timestamp || block.time,
    };
  }
}

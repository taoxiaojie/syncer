'use strict';
const SatoshisWatcher = require('./satoshi');
const Client = require('bitcoin-core');
const blockexplorer = require('blockchain.info/blockexplorer')
const Socket = require('blockchain.info/Socket')
const BigNumber = require('bignumber.js');

module.exports = class BtcWatcher extends SatoshisWatcher {
  constructor(options) {
    super(options);
    const {host, block_port} = options;
    this.baseUrl = `http://${host}:${block_port}/rest/`;
    this.client = new Client({
      host: host,
      username: 'bdeals',
      password: 'bdeals_bitcoin',
      port: block_port,
    });
  }

  async beforeInit() {
    this.connect();
  }

  get tokenName() {
    return 'btc';
  }

  handleSocketTransaction(tx) {
    const {inputs, out, hash} = tx;
    for (let item of out) {
      const {addr, value, n} = item;
      const amount = BigNumber(value).div(1e8).toString();
      const transaction = {
        txid: hash,
        address: addr,
        value: amount,
        output_no: n,
        block_height: 0,
        block_hash: '',
      };
      this.publishPendingTx(transaction);
    }
  }

  connect() {
    const socket = new Socket();
    socket.onClose(() => {
      setTimeout(() => {
        this.connect();
      }, 1000);
    });

    socket.onTransaction((tx) => {
      this.handleSocketTransaction(tx);
    });

    socket.onBlock(block => {
      // const fs = require('fs');
      // const path = require('path');
      // fs.appendFileSync(path.join(__dirname, 'block.json'), JSON.stringify(block, null, 2));
      // console.log(block);
    });
  }

  async getLatestBlock() {
    try {
      return super.getLatestBlock();
    } catch (err) {
      const data = await blockexplorer.getLatestBlock();
      return await this.getBlockByHash(data.hash);
    }
  }

  async getBlockHashByHeight(height) {
    return await this.client.getBlockHash(height);
  }

  async getTx(txid) {
    return await this._request(`/tx/${txid}.json`);
  }

  handleTx(tx) {
    const {txid, vin, vout, block_hash, block_height} = tx;
    for (let item of vout) {
      const {scriptPubKey, value, n} = item;
      const addresses = scriptPubKey && scriptPubKey.addresses;
      if (!(addresses && addresses.length)) {
        continue;
      }

      for (let address of addresses) {
        if (scriptPubKey.type === 'pubkey') {
          continue;
        }

        address = this.getAddress(address, scriptPubKey.type);

        const data = {
          block_height,
          txid,
          block_hash: block_hash,
          value,
          address,
          output_no: n,
        };

        this.publishTx(data);
      }
    }
  }

  handlePendingTx(tx) {
    const {txid, vin, vout} = tx;
    for (let item of vout) {
      const {scriptPubKey, value, n} = item;
      const addresses = scriptPubKey && scriptPubKey.addresses;
      if (!(addresses && addresses.length)) {
        continue;
      }

      for (let address of addresses) {
        if (scriptPubKey.type === 'pubkey') {
          continue;
        }

        address = this.getAddress(address, scriptPubKey.type);

        const data = {
          block_height: 0,
          txid,
          block_hash: '',
          value,
          address,
          output_no: n,
        };

        this.publishPendingTx(data, tx);
      }
    }
  }

}

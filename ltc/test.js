'use strict';
const urllib = require('urllib');
const BaseSyncer = require('../lib/base_syncer');
const debug = require('debug')('ltc');
const Config = require('lcoin/lib/node/config');
const util = require('lcoin/lib/utils/util');
const co = require('lcoin/lib/utils/co');
const Client = require('lcoin/lib/http/client');
const Wallet = require('lcoin/lib/http/wallet');
const Amount = require('lcoin/lib/btc/amount');

class Syncer extends BaseSyncer {
  constructor(options) {
    super(options);
    this.db = new this.DataBase(this.assetsName);
  }

  get assetsName() {
    return 'ltc';
  }

  get assets_type() {
    return 4;
  }

  get confirmNumber() {
    return 6;
  }

  get minAmount() {
    return 0.01;
  }

  async beforeInit() {
    this.config = new Config('lcoin');
    this.config.load({
      argv: true,
      env: true
    });
    this.client = new Client({
      uri: this.config.str(['url', 'uri']),
      apiKey: this.config.str('api-key'),
      network: this.config.str('network')
    });
  }

 formatBlock(block) {
    return {
      hash: block.hash,
      height: block.height,
      nextblockhash: block.nextblockhash,
      timestamp: block.ts * 1000,
    };
  }

  getBlockTransactions(block) {
	   return block.txs;
  }

  // 同步未确认的区块
  async syncPenddingTransactions() {
    while (true) {
      await this.sleep('3s');
      try {
        const data = await this.client.getMempool();
        if (data && data.length) {
      	  for(let txid of data) {
            const tx = await this.getTx(txid);
            this.handleTx(tx, 0);
      	  }
        }
      } catch (err) {
        console.log(err);
      }
    }
  }

  async getLatestBlock() {
    const method = 'getblockchaininfo';
    const params = [];
    const info = await this.client.rpc.execute(method, params);
    const hash = info.bestblockhash;
    return await this.getBlockByHash(hash);
  }

  handleTx(transaction, height, blockHash) {
    const {outputs, hash} = transaction;
    for (let out of outputs) {
      const {address, value} = out;
      const amount = this.formatAmount(value);
      if (parseFloat(amount) < this.minAmount) {
        continue;
      }

      try {
        const data = {
          height,
          txid: hash,
          block: blockHash,
          amount,
          address,
        };
        debug('transaction %j', data);
        this.postTransaction(data);
      } catch (err) {
        console.log(JSON.stringify(transaction, null, 2));
        console.log(err);
      }
    }
  }

  async getTx(hash) {
    const tx = await this.client.getTX(hash);
    return tx;
  }

  async getBlock(lastBlock) {
   return await this.getBlockByHeight(lastBlock.height+1);
  }

  async getBlockByHeight(height) {
    const block = await this.client.getBlock(height);
    return block;
  }

  async getBlockByHash(hash) {
    if (!hash) {
      return;
    }
    const block = await this.client.getBlock(hash);
    return block;
  }
}

new Syncer();

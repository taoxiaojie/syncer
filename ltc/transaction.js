'use strict';
const litecore = require('litecore-lib');
const fs = require('fs');
const path = require('path');
const keysFile = path.join(__dirname, 'key.txt');
const addressFle = path.join(__dirname, 'address.txt');
const PrivateKey = litecore.PrivateKey;

// let count = 0;
// while (true) {
//   count++;
//   const privateKey = new litecore.PrivateKey();
//   const key = privateKey.toString();
//   const address = privateKey.toAddress().toString();
//   fs.appendFileSync(keysFile, `${address}:${key}\n`);
//   fs.appendFileSync(addressFle, address + '\n');
//   if (count % 10000 === 0) {
//     console.log(count);
//   }
// }

const scale = 100000000;
var Script = litecore.Script;

const utxos = [
  {
    "txId" : "2794efb6f2d7e9e8874070a90a919e768f7be86061e809a2a6787347184132c4",
    "outputIndex" : 1,
    "address" : "LRmi67vwYaZnjUPkLjaegD5tmov2V125w2",
    "script" : "76a91447d23bcde2775344b9ac102ae5306ed03ad2fc1b88ac",
    "satoshis" : 0.09999999 * scale
  },
  {
    "txId" : "5a7747e690ade4fc82abbf676b42f61a5ff56db24235df60ebc6b92af7980b08",
    "outputIndex" : 0,
    "address" : "LRmi67vwYaZnjUPkLjaegD5tmov2V125w2",
    "script" : "76a91447d23bcde2775344b9ac102ae5306ed03ad2fc1b88ac",
    "satoshis" : 0.798 * scale
  },
  {
    "txId" : "d313e29dc2d23b0bb09734f71b82fb0d31d3d17e7b494ce20a8927f2a73cafce",
    "outputIndex" : 0,
    "address" : "LhLPAVJ4hRx55ehdtqRkLwBpvfoaMN7hvD",
    "script" : "76a914f28a0cb0fd7c8a0cdbf2fbb470a74ad518460f5f88ac",
    "satoshis" : 0.09934981 * scale
  },
];

let total = 0;
const fee = 0.00002509 * scale;
const withdraw = 0.09 * scale;
for (let utxo of utxos) {
  total += utxo.satoshis;
}

var transaction = new litecore.Transaction()
  .from(utxos)
  .fee(fee)
  .to([
    {
      address: 'LPzTACF1ZQRGLTVPgNviEJYMJunweneqvm',
      satoshis: total - withdraw - fee
    },
    {
      address: 'LbE6poqjpKEHrQiP9CoBm8jgZ3a1DFmPkd',
      satoshis: withdraw
    }
  ])
  .change('LPzTACF1ZQRGLTVPgNviEJYMJunweneqvm')
  .sign([
    new PrivateKey('1'),
    new PrivateKey('1'),
    new PrivateKey('1'),
  ]
);

console.log(transaction.toString());

'use strict';
const ERC20 = require('../lib/erc20/erc20.json');
const tokenMap = require('../lib/erc20/tokens');
const Web3 = require('web3');
const utils = Web3.utils;
const assert = require('assert');
const BN = require('bignumber.js');
const ETHWallet = require('./eth');
const gasPrice = utils.toWei('0.000000006', 'ether');
/**
 * Represents a wallet instance.
 */
module.exports = class Erc20Wallet extends ETHWallet {
  constructor (options) {
    super(options);
    const web3 = new Web3(options.url);
    const contracts = {};
    const tokens = {};
    for (let address in tokenMap) {
      const token = tokenMap[address];
      const name = token.name.toUpperCase();
      contracts[name] = new web3.eth.Contract(ERC20, utils.toChecksumAddress(address));
      tokens[name] = token;
    }
    this.tokens = tokens;
    this.web3 = web3;
    this.contracts = contracts;
  }

  async transfer({to, value, token}) {
    assert(token, 'token is required');
    token = token.toUpperCase();
    const contract = this.contracts[token];
    assert(contract, `Invalid token ${token}`);

    const tokenInfo = this.tokens[token];
    const amount = BN(value).times(Math.pow(10, tokenInfo.decimals));
    const addresses = this.getAddresses();
    addresses[0] = address;
    let targetWallet, totalBalance;

    for (let item of addresses) {
      const balance = await contract.methods.balanceOf(item).call();
      if (amount.lte(balance)) {
        targetWallet = item;
        totalBalance = balance;
        break;
      }
    }
    // 热钱包资金不足
    if (!targetWallet) {
      return;
    }
    const web3 = this.web3;
    const nonce = await web3.eth.getTransactionCount(targetWallet, web3.eth.defaultBlock.pending);

    const method = contract.methods.transfer(to, amount.toFixed(0));
    const data = method.encodeABI();
    // const gasPrice = await web3.eth.getGasPrice();

    const rawtx = super.signTransaction({
      nonce: web3.utils.toHex(nonce),
      from: targetWallet,
      to: tokenInfo.address,
      value: '0x00',
      data,
      gasLimit: web3.utils.toHex(101000),
      gasPrice: web3.utils.toHex(gasPrice),
    });

    const ret = await web3.eth.sendSignedTransaction(rawtx);
    console.log(ret);
  }
}

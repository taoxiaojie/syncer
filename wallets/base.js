'use strict';
const { addHexPrefix } = require('ethereumjs-util');
const { fromExtendedKey } = require('ethereumjs-wallet/hdkey');
const EthereumTx = require('ethereumjs-tx')
const EthSigUtil = require('eth-sig-util');
const Mnemonic = require('bitcore-mnemonic');
const assert = require('assert');
const sleep = require('ko-sleep');
const BASE_PATH = `m/1024'/1024'/1024'/1024`;
const urllib = require('urllib');

module.exports = class BaseWallet {
  constructor(options={}) {
    assert(options.mnemonic, 'mnemonic is required');
    options.count = parseInt(options.count) || 100;
    options.from = parseInt(options.from) || Math.pow(2, 30);

    const { xprivkey } = new Mnemonic(options.mnemonic).toHDPrivateKey();
    this._hdKey = fromExtendedKey(xprivkey);
    this._root = this._hdKey.derivePath(BASE_PATH);
    this._children = [];
    this._keys = {};
    this.generateAddresses(options.count, options.from);
  }

  async sleep(time) {
    await sleep(time);
  }

  async request(url, options={}) {
    try {
      const res = await urllib.request(url, {
        ...options,
        dataType: 'json',
      });
      return res.data;
    } catch (err) {
      return null;
    }
  }

  generateAddresses (count, from) {
    const newKeys = this._deriveNewKeys(count, from);
    return newKeys.map(k => k.address)
  }

  getRandomAddress() {
    const addresses = this.getAddresses();
    const index = parseInt(Math.random()*Date.now());
    return addresses[index%addresses.length];
  }

  getAddresses () {
    return this._children.map(k => k.address)
  }

  getPrivateKey(address) {
    return this._keys[address];
  }

  toAddress() {

  }

  signTransaction() {
    throw new Error('signTransaction not implemented');
  }

  transfer(to, value) {
    throw new Error('transfer not implemented');
  }

  _deriveNewKeys (count, from = 0) {
    let index = 0;
    while (index < count) {
      const child = this._root.deriveChild(from + (index++)).getWallet();
      const address = this.toAddress(child);
      this._keys[address] = child.getPrivateKey();
      this._children.push({
        wallet: child,
        address,
      });
    }

    return this._children.slice(0);
  }
}

'use strict';
const SatoshiWallet = require('./satoshi');
const core = require('litecore-lib');
const {PrivateKey, PublicKey, Address, crypto} = core;

module.exports = class LTCWallet extends SatoshiWallet {
  constructor(options) {
    super(options);
  }

  toAddress(node) {
    const privatekey = node.getPrivateKey();
    return new PrivateKey(privatekey).toAddress();
  }
}

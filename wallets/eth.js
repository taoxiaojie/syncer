'use strict';
const { addHexPrefix } = require('ethereumjs-util');
const EthereumTx = require('ethereumjs-tx');
const BN = require('bignumber.js');

const EthSigUtil = require('eth-sig-util');
const Web3 = require('web3');
const web3 = new Web3('https://mainnet.infura.io/qexcS4J1PiCKn9WlivBs');
const gasPrice = web3.utils.toWei('0.000000006', 'ether');
const gasLimit = 21000;
const gas = BN(gasPrice).times(gasLimit).toString();

const BaseWallet = require('./base');
/**
 * Represents a wallet instance.
 */
module.exports = class ETHWallet extends BaseWallet {
  constructor (options) {
    super(options);
  }

  /**
   * Sign transaction data.
   *
   * @param  {String} from From address
   * @param  {String} [to] If omitted then deploying a contract
   * @param  {Number} value Amount of wei to send
   * @param  {String} data Data
   * @param  {Number} gasLimit Total Gas to use
   * @param  {Number} gasPrice Gas price (wei per gas unit)
   * @param  {String} chainId Chain id
   *
   * @return {String} Raw transaction string.
   */
  signTransaction ({ nonce, from, to, value, data, gasLimit, gasPrice, chainId }) {
    const { wallet } = this._children.find(({ address }) => from === address) || {}

    // if (!wallet) {
    //   throw new Error('Invalid from address')
    // }

    const tx = new EthereumTx({
      nonce, to, value, data, gasLimit, gasPrice,
      chainId: 1,
    });

    tx.sign(wallet.getPrivateKey());

    return addHexPrefix(tx.serialize().toString('hex'));
  }

  toAddress(node) {
    return addHexPrefix(node.getAddress().toString('hex'));
  }
}

'use strict';
const core = require('bitcore-lib');
const {Transaction, Script, Address} = core;
const BN = require('bignumber.js');
const SatoshiWallet = require('./satoshi');
const is = require('is-type-of');

module.exports = class BTCWallet extends SatoshiWallet {
  constructor(options) {
    super(options);
  }

  getScript(address) {
    return new Script(new Address(address)).toHex();
  }

  getAddressFromScript(script) {
    return Address.fromScript(script).toString();
  }

  getTransaction() {
    return new Transaction();
  }

  // 获取多少金额的utxo
  async getUtxo(value) {
    const addresses = this.getAddresses();
    let total = BN(0);
    const list = [];
    let count = 0;
    for (let address of addresses) {
      let items = await this.queryUtxo(address);
      // 先使用大的utxo
      items.sort((a, b) => {
        return parseFloat(b.value) - parseFloat(a.value);
      });
      for (let item of items) {
        list.push(item);
        count++;
        total = total.plus(item.value);
        if (count >= 20 || total.gte(value)) {
          return {
            total: total.toString(),
            list,
          }
        }
      }
    }
    return {
      total: total.toString(),
      list,
    }
  }

  async queryUtxo(address) {
    try {
      return super.queryUtxo(address);
    } catch (err) {
      console.log(err);
    }
  }

  async queryUtxoFromApi(address) {
    let data = await this.request(`https://insight.bitpay.com/api/addr/${address}/utxo`);
    if (data.length) {
      return data.map(item => {
        return {
          txid: item.txid,
          address,
          value: item.amount,
          output_no: item.vout,
        }
      });
    }

    if (is.nullOrUndefined(data)) {
      const res = await this.request(`https://chain.so/api/v2/get_tx_unspent/BTC/${address}`);
      if (res.status === 'success') {
        const tx = res.data.txs;
        if (tx && tx.length) {
          return tx.map(item => {
            return {
              txid: item.txid,
              output_no: item.output_no,
              address,
              value: item.value,
            }
          });
        }
      }
    }
  }
}

'use strict';
const BTCWallet = require('./btc');
const EthHdWallet = require('./eth');
const ERC20Wallet = require('./erc20');
const USDTWallet = require('./usdt');
const LTCWallet = require('./ltc');
const { generateMnemonic } = require('eth-hd-wallet')

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.stack);
});

// const mnemonic = generateMnemonic();

const mnemonic = 'tone purity ribbon timber problem seek furnace this luggage rhythm mountain foot';
const wallet1 = new BTCWallet({
  mnemonic,
});

const usdtWallet = new USDTWallet({
  mnemonic,
});
const wallet2 = new ERC20Wallet({
  mnemonic,
  url: 'http://47.75.55.45:8545',
});


async function test() {
  try {
    // await usdtWallet.transfer()
    // wallet2.transfer({
    //   token: 'ETU',
    //   value: 1,
    //   to: '0x54452Ee286086E07d72c4d54Cc2F00FB75Ab9187',
    // });
    await wallet1.transfer([
      {
        address: '34cH6yTLNwhz3oY9w1a9vWGUVJd943JqNo',
        value: '0.1',
      },
      {
        address: '1Po1oWkD2LmodfkBYiAktwh76vkF93LKnh',
        value: '0.3',
      }
    ])
  } catch (err) {
    console.log(err);
  }
}

test()

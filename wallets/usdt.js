'use strict';
const SatoshiWallet = require('./satoshi');
const core = require('litecore-lib');
const {PrivateKey, PublicKey, Address, crypto} = core;
const BN = require('bignumber.js');

module.exports = class USDTWallet extends SatoshiWallet {
  constructor(options) {
    super(options);
  }

  signTransaction() {

  }

  async getBalanceByAddress(addresses) {
    if (!Array.isArray(addresses)) {
      addresses = [addresses];
    }
    return await this.getBalanceFromOmni(addresses);
  }

  async getBalanceFromOmni(addresses) {
    const data = addresses.map(item => {
      return `addr=${item}`;
    }).join('&');

    try {
      const result = await this.request(`https://api.omniwallet.org/v2/address/addr/`, {
        data,
        method: 'post',
      });
      const balance = {};
      for (let item of addresses) {
        const balanceInfo = result[item].balance;
        const ret = {};
        balanceInfo.forEach(b => {
          const id = parseInt(b.id);
          const value = parseInt(b.value);
          if (id === 0) {
            ret.btc = value;
          } else if (id === 31) {
            ret.usdt = value;
          }
        });
        ret.btc = ret.btc || 0;
        ret.usdt = ret.usdt || 0;
        balance[item] = ret;
      }
      return balance;
    } catch (err) {
      console.log(err);
      await this.sleep('10s');
      return await this.getBalanceFromOmni(addresses);
    }
  }

  async getPaymentAccount(value) {
    const addresses = this.getAddresses();
    let addr = [];
    let index = 0;
    while (index < addresses.length) {
      addr.push(addresses[index++])
      if (addr.length >= 10) {
        const balance = await this.getBalanceByAddress(addr);
        for (let address of addr) {
          const item = balance[address];
          if (item.btc > 0 && item.usdt >= value) {
            return address;
          }
        }
        addr = [];
      }
    }
  }

  async transfer(to, value) {
    const address = await this.getPaymentAccount();
    if (!address) {
      return console.log('usdt热钱包余额不足');
    }
    const tx = new Transaction();

    const fundValue     = 546 // dust
    const feeValue      = 1000

    const unspents = await this.getUtxo(fundValue + feeValue);

    const totalUnspent  = unspents.reduce((summ, { satoshis }) => summ + satoshis, 0)
    const skipValue     = totalUnspent - fundValue - feeValue

    if (totalUnspent < feeValue + fundValue) {
      throw new Error(`Total less than fee: ${totalUnspent} < ${feeValue} + ${fundValue}`)
    }

    const utxoSpend = [];

    unspents.forEach(({ txid, output_no, address, satoshis, ...rest }) => {
      utxoSpend.push({
        "txId" : txid,
        "outputIndex" : output_no,
        "address" : address,
        "script" : new Script.fromAddress(new Address(address)),
        "satoshis" : satoshis,
      });
    });

    tx.from(utxoSpend);

    const simple_send = [
      "6f6d6e69", // omni
      "0000",     // version
      "00000000001f", // 31 for Tether
      ('0000000000000000' + BN(3).times(1e8).toString(16).toUpperCase()).slice(-16)
    ].join('')

    const data = Buffer.from(simple_send, "hex");

    tx.to(to, fundValue);
    tx.addData(data);
    tx.to(address, skipValue);

    const tx = this.signTransaction(tx);
    console.log(tx.serialize());

  }
}

'use strict';
const BN = require('bignumber.js');
const BaseWallet = require('./base');
const core = require('bitcore-lib');
const {PrivateKey, PublicKey, Address, crypto} = core;
const scale = 1e8;
const fee = 0.0001; // 给一个地址转账的平均费用

module.exports = class SatoshiWallet extends BaseWallet {
  constructor(options) {
    super(options);
  }

  toAddress(node) {
    const privatekey = node.getPrivateKey();
    return new PrivateKey(privatekey).toAddress();
  }

  signTransaction(tx) {
    const inputs = tx.inputs;
    const privateKeys = inputs.map(input => {
      const address = this.getAddressFromScript(input.output.script);
      return this.getPrivateKey(address);
    });
    return tx.sign(privateKeys);
  }

  queryUtxo() {
    return [
      {
        "address": "1Cc9hozHG8Ed4meQo31KT7C3DZTxupt4fY",
        "txid": "7099144b0b0386297b1a8bd807bbe6ba7573c96b32c2f8c3ee6cb2e10406d6f9",
        "output_no": 0,
        "scriptPubKey": "76a9147f4f352c0191471a0bac12ec554a10bf4b459e8b88ac",
        "value": 1.63046334,
        "satoshis": 163046334,
        "height": 542510,
        "confirmations": 3
      },
      {
        "address": "1HK4dHTyfs9tFbju9nA3w9WX4VFjDfYWZM",
        "txid": "b20034f30dabb25a96406a0629e35d3f45ead8e4f372c305db38161be4902c9c",
        "output_no": 0,
        "scriptPubKey": "76a914b2ec86be148c591ca6d6ed268c22f6f5939d016388ac",
        "value": 0.94190817,
        "satoshis": 94190817,
        "height": 542511,
        "confirmations": 2
      },
    ]
  }

  toSatoshi(value) {
    return BN(value).times(scale).toNumber();
  }

  // 获取多少金额的utxo
  async getUtxo(value) {
    const addresses = this.getAddresses();
    let total = BN(0);
    const list = [];
    let count = 0;
    for (let address of addresses) {
      let items = await this.queryUtxo(address);
      // 先使用大的utxo
      items.sort((a, b) => {
        return parseFloat(b.value) - parseFloat(a.value);
      });
      for (let item of items) {
        list.push(item);
        count++;
        total = total.plus(item.value);
        if (count >= 20 || total.gte(value)) {
          return {
            total: total.toString(),
            list,
          }
        }
      }
    }
    return {
      total: total.toString(),
      list,
    }
  }

  // 合并转账，收款地址可能一样
  mergeOutputs(outputs) {
    const transformMap = {};
    for (let item of outputs) {
      const {address, value} = item;
      if (transformMap[address]) {
        transformMap[address] = transformMap[address].plus(value);
      } else {
        transformMap[address] = BN(value);
      }
    }

    return Object.keys(transformMap).map(address => {
      return {
        address,
        value: transformMap[address].toString(),
      }
    });
  }

  // 给多个用户转账
  async transfer(outputs) {
    outputs = this.mergeOutputs(outputs);
    outputs.sort((a, b) => {
      return parseFloat(a.value) - parseFloat(b.value);
    });

    // 计算出需要转账的总金额
    let transferValue = BN(0);
    for (let output of outputs) {
      transferValue = transferValue.plus(output.value).plus(fee);
    }

    const ret = await this.getUtxo(transferValue.toString());
    const list = ret.list;
    const left = ret.total;
    let transferList = [];
    const utxoSpend = [];

    // 剩余utxo不够用
    if (transferValue.gt(left)) {
      transferValue = BN(0);
      for (let output of outputs) {
        transferValue = transferValue.plus(output.value).plus(fee);
        if (transferValue.lte(left)) {
          transferList.push(output);
        } else {
          break;
        }
      }
    } else {
      transferList = outputs;
    }

    if (!transferList.length) {
      return;
    }

    let value = BN(0);
    for (let item of list) {
      utxoSpend.push({
        txId: item.txid,
        outputIndex: item.output_no,
        address: item.address,
        script: this.getScript(item.address),
        satoshis: this.toSatoshi(item.value),
      });

      value = value.plus(item.value);
      if (value.gte(transferValue)) {
        break;
      }
    }

    const tranformFee = BN(fee).times(transferList.length).times(scale).toNumber();

    let transaction = this.getTransaction().from(utxoSpend).fee(tranformFee);
    for (let item of transferList) {
      transaction = transaction.to(item.address, this.toSatoshi(item.value));
    }

    transaction = transaction.change(this.getRandomAddress());

    transaction = this.signTransaction(transaction);
    const rawtx = transaction.serialize();
    const hash = transaction.hash;
    console.log(transaction.toJSON());
  }
}

// 处理比特币系列的交易记录
'use strict'
const Base = require('./base');
const urllib = require('urllib');
const SatoshisHandler = require('./satoshi');

module.exports = class LTChandler extends SatoshisHandler {
  constructor(options) {
    super(options);
  }

  getAddress(index) {
    return this.addressMaker.getAddress(4, index);
  }

  get tokenName() {
    return 'ltc';
  }

  get minAmount() {
    return 0.05;
  }

  get confirmNumber() {
    return 12;
  }

  get assets_type() {
    return 4;
  }
}

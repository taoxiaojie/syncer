// 处理比特币系列的交易记录
'use strict'
const Base = require('./base');
const urllib = require('urllib');
const SatoshisHandler = require('./satoshi');

module.exports = class BCHandler extends SatoshisHandler {
  constructor(options) {
    super(options);
  }

  getAddress(index) {
    return this.addressMaker.getAddress(1, index);
  }

  get tokenName() {
    return 'bch';
  }

  get minAmount() {
    return 0.005;
  }

  get confirmNumber() {
    return 1;
  }

  get assets_type() {
    return 3;
  }
}

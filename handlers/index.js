'use strict';

module.exports = {
  btc: require('./btc'),
  bch: require('./bch'),
  ltc: require('./ltc'),
  eth: require('./eth'),
  usdt: require('./usdt'),
};

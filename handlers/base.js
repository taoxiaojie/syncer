'use strict';

const sleep = require('ko-sleep');
const path = require('path');
const mkdirp = require('mkdirp');
const fs = require('fs');
const os = require('os');
const ms = require('ms');
const debug = require('debug');
const Base = require('sdk-base');
const LRU = require("lru-cache");
const homedir = require('../lib/homedir');
const addressMaker = require('../lib/address_maker');
const mongo = require('../lib/mongo');
const NodeRSA = require('node-rsa');
const urllib = require('urllib');
const CryptoJS = require("crypto-js");
const Redis = require('ioredis');
const is = require('is-type-of');
// const rsa = new NodeRSA(Buffer.from(process.env.privateKey, 'hex').toString());
const assert = require('assert');
const dataId = 'withdraw';
const group = 'DEFAULT_GROUP';
const api = require('../lib/api');

module.exports = class BaseHandler extends Base {
  constructor(options={}) {
    super({
      initMethod: 'init',
    });
    this.redis = new Redis({
      host: options.host || 'localhost',
      port: options.port || 6379,
    });
    this.watcher = options.watcher;
    this.addressMaker = addressMaker;
  }

  async init() {
    this.watcher.on('transaction', (transaction, raw) => {
      if (is.nullOrUndefined(transaction.block_height)) {
        console.trace(transaction);
      }
      this.handleTransaction(transaction, raw);
    });

    this.watcher.on('pending_transaction', (transaction, raw) => {
      if (is.nullOrUndefined(transaction.block_height)) {
        console.trace(transaction);
      }
      this.handlePendingTransaction(transaction, raw);
    });

    this.watcher.on('new_block', block => {
      this.latestBlock = block;
      console.log('new block', block.height);
    });

    this.makeAddress();
    this.startCheckConfirm();
  }

  getAddress(index) {

  }

  async makeAddress() {
    let index = 0;
    const size = 100000;
    while (index < size) {
      const address = this.getAddress(index++);
      await this.redis.set(`watch_address:${address}`, '1');
    }
  }

  async isAddressWatched(address) {
    const ret = await this.redis.get(`watch_address:${address}`);
    return !!(ret && parseInt(ret) === 1);
  }

  async startCheckConfirm() {
    while (true) {
      const latestBlock = this.latestBlock;
      if (!latestBlock) {
         await sleep('10s');
         continue;
      }
      try {
        await this.doCheck();
      } catch (err) {
        console.log(err);
      }
      await sleep('10s');
    }
  }

  async doCheck() {
    const depositModel = this.depositModel;
    const latestBlock = this.latestBlock;
    const height = latestBlock.height;
    const confirmNumber = this.confirmNumber;
    const records = await depositModel.find({
      status: { $gt: 1 },
      block_height: {
        $lte: height-confirmNumber + 1,
      },
      value: {
        $gte: this.minAmount,
      },
      confirmed: 0,
    });

    if (records.length) {
      for (let item of records) {
        if (parseFloat(item.value) >= this.minAmount && item.block_height) {
          await api.postConfirmedTransaction({
            assets_type: this.assets_type,
            txid: item.txid,
            block: item.block_hash,
          });
        }

        await depositModel.updateOne({
          txid: item.txid,
          address: item.address,
        }, {
          confirmed: 1,
        });
      }
    }
  }

  // 该交易已经在区块链上确定
  async handleTransaction(transaction) {
    try {
      const isWatched = await this.isAddressWatched(transaction.address);
      if (!isWatched) {
        return;
      }
      console.log(transaction);
      this.emit('transaction', transaction);
      await this.confirmTransaction(transaction);
    } catch (err) {
      console.log(err);
    }
  }

  async handlePendingTransaction(transaction, raw) {
    try {
      const isWatched = await this.isAddressWatched(transaction.address);
      if (!isWatched) {
        return;
      }

      await this.createTransaction(transaction);
      if (parseFloat(transaction.value) >= this.minAmount) {
        await api.postPendingTransaction({
          assets_type: this.assets_type,
          address: transaction.address,
          amount: transaction.value,
          txid: transaction.txid,
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  async createTransaction(transaction) {
    const depositModel = this.depositModel;
    const record = await depositModel.findOne({
      txid: transaction.txid,
      address: transaction.address,
    });

    if (!(record && record.txid)) {
      await new depositModel(transaction).save();
    }
  }

  async confirmTransaction(transaction) {
    const depositModel = this.depositModel;
    const conditions = {
      txid: transaction.txid,
      address: transaction.address,
    };

    if (!is.nullOrUndefined(transaction.output_no)) {
      conditions.output_no = transaction.output_no;
    }

    const record = await depositModel.findOne(conditions);

    if (!(record && record.txid)) {
      return await new depositModel({
        ...transaction,
        status: 2,
      }).save();
    }

    if (parseInt(record.status) === 1) {
      await depositModel.updateOne(conditions, {
        ...transaction,
        status: 2,
      });
    }  else {
      await depositModel.updateOne(conditions, {
        ...transaction,
        status: record.status,
      });
    }
  }
}

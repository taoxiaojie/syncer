// 处理比特币系列的交易记录
'use strict'
const Base = require('./base');
const urllib = require('urllib');
const mongo = require('../lib/mongo');

module.exports = class ETHHandler extends Base {
  constructor(options) {
    super(options);
    this.depositModel = mongo.getDepositModel();
  }

  getAddress(index) {
    return this.addressMaker.getAddress(2, index);
  }

  get assets_type() {
    return 2;
  }

  get minAmount() {
    return 0.01;
  }

  get confirmNumber() {
    return 12;
  }

  async init() {
    await super.init();
    this.watcher.on('erc20_transaction', transaction => {
      this.handleTransaction(transaction);
    });
  }

  // 该交易已经在区块链上确定
  handleTransaction(transaction) {
    const DepositModel = this.depositModel
    new DepositModel(transaction).save();
  }

  handlePendingTransaction(transaction) {
    const depositModel = this.depositModel;
    new depositModel(transaction).save();
  }
}

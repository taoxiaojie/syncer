// 处理比特币系列的交易记录
'use strict'
const Base = require('./base');
const urllib = require('urllib');
const mongo = require('../lib/mongo');

module.exports = class USDTHandler extends Base {
  constructor(options) {
    super(options);
    this.depositModel = mongo.getDepositModel();
  }

  getAddress(index) {
    return this.addressMaker.getAddress(1, index);
  }

  get assets_type() {
    return 5;
  }

  get tokenName() {
    return 'USDT';
  }

  get minAmount() {
    return 10;
  }

  get confirmNumber() {
    return 2;
  }

  handleTransaction(transaction) {
    super.handleTransaction({
      ...transaction,
      token: this.tokenName,
    });
  }

  handlePendingTransaction(transaction) {
    super.handlePendingTransaction({
      ...transaction,
      token: this.tokenName,
    });
  }
}

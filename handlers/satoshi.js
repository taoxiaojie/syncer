// 处理比特币系列的交易记录
'use strict'
const Base = require('./base');
const urllib = require('urllib');
const mongo = require('../lib/mongo');

module.exports = class SatoshisHandler extends Base {
  constructor(options) {
    super(options);
    this.depositModel = mongo.getBitDepositModel(this.tokenName);
  }
}

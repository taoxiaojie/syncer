'use strict';
const urllib = require('urllib');
const BaseSyncer = require('../lib/bit_syncer');
const debug = require('debug')('btc');
const Client = require('bitcoin-core');
const blockchain = require('blockchain.info')
const block_port = process.env.block_port || 8332;

module.exports = class Syncer extends BaseSyncer {
  constructor(options) {
    super(options);
    this.baseUrl = `http://${this.host}:${block_port}/rest/`;
    this.client = new Client({
      host: this.host,
      username: 'bdeals',
      password: 'bdeals_bitcoin',
      port: block_port
    });
    this.db = new this.DataBase(this.assetsName);
  }

  get assetsName() {
    return 'btc';
  }

  get assets_type() {
    return 1;
  }

  get confirmNumber() {
    return parseInt(process.env.confirmNumber) || 2;
  }

  get minAmount() {
    return 0.0025;
  }

  async isSpent(txid, output_no) {
    const res = await urllib.request(`https://chain.so/api/v2/is_tx_spent/BTC/${txid}/${output_no}`);
    const data = res && res.data;
    if (data && data.status === 'success') {
      return data.data.is_spent;
    }
  }
}

'use strict';
const Web3 = require('web3');
const web3 = new Web3('https://mainnet.infura.io/qexcS4J1PiCKn9WlivBs');
// const web3 = new Web3('http://192.168.0.5:32813')
// const web3 = new Web3('http://127.0.0.1:8545')

const txid = '0x91f8b6afa8563ca90df52d9a13a40f5a9f32baee769bbe86fe49cf9498b9b886';
const ERC20 = require('../lib/erc20.json');

const contract = new web3.eth.Contract(ERC20, '0x8dd5fbce2f6a956c3022ba3663759011dd51e73e');

// web3.eth.getTransactionReceipt(txid).then(ret => {
//   console.log(JSON.stringify(ret, null, 2));
// });

contract.getPastEvents('Transfer', {
  fromBlock: 6359748,
  toBlock: 6359750,
}).then(console.log)

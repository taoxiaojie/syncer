'use strict';
const Web3 = require('web3');
const BaseSyncer = require('../lib/base_syncer');
const debug = require('debug')('eth');

module.exports = class EthSyncer extends BaseSyncer {
  constructor(options) {
    super(options);
  }

  get utxo() {
    return false;
  }

  get minAmount() {
    return super.minAmount || 0.01;
  }

  get assetsName() {
    return 'eth';
  }

  get assets_type() {
    return 2;
  }

  get confirmNumber() {
    return parseInt(process.env.confirmNumber) || 15;
  }

  get minAmount() {
    return 0.01;
  }

  getTransactionByHash(txid) {
    const provider = this.getProvider();
    if (provider) {
      return new Promise(resolve => {
        provider.eth.getTransaction(txid, (err, data) => {
          if (err) {
            console.log(err);
          }
          if (data) {
            resolve(Object.assign(data,{
              hash: data.blockHash,
              height: data.blockNumber,
            }));
          } else {
            resolve();
          }
        });
      });
    } else {
      console.log('no provider');
    }
  }

  // 同步未确认的区块
  async syncPenddingTransactions() {
    while (true) {
      await this.sleep('3s');
      const provider = this.getProvider();
      if (!provider) {
        continue;
      }
      const block = await this._getBlock(provider, 'pending');
      const transactions = block && block.transactions
      if (transactions && transactions.length) {
        for (let tx of transactions) {
          this.handleTx(tx, 0, null);
        }
      }
    }
  }

  async beforeInit() {
    this.checkHeight = '5684588';
    this.checkHash = '0x2642a15f1891ce294a412b549dfebaeac243cac7eb9e2050de6ebd890b22c8ee';
    this.providers = [];
    this.index = 0;
    const providers = this.options.providers;
    if (providers) {
      for (let item of providers) {
        await this.addProvider(item);
      }
    }
  }

  async getLatestBlock() {
    const provider = this.getProvider();
    if (!provider) {
      return;
    }
    const block = await this._getBlock(provider, 'latest');
    // block.transactions = await this.getTransactionsByBlock(block);
    return block;
  }

  async addProvider(url) {
    const provider = new Web3(url);
    const block = await this._getBlock(provider, this.checkHeight, false);
    debug(`added provider ${url}, block is `, block);
    this.providers.push(provider);
  }

  async getTransactionsByBlock(block) {
    const transactions = block.transactions;
    const txs = [];
    for (let txid of transactions) {
      const tx = await this.getTransactionByHash(txid);
      txs.push(tx);
    }
    return txs;
  }

  handleTx(transaction, height, blockHash) {
    const {to, value, hash, from} = transaction;
    let amount = this.formatAmount(Web3.utils.fromWei(value));
    if (parseFloat(amount) < this.minAmount) {
      return;
    }
    const data = {
      height,
      txid: hash,
      block: blockHash,
      amount,
      address: to,
      from,
    };
    this.postTransaction(data);
    this.handleTxOut(data);
  }

  getProvider() {
    const providers = this.providers;
    const index = (this.index++)%providers.length;
    return providers[index];
  }

  async getBlock(lastBlock) {
    const provider = this.getProvider();
    if (!provider) {
      return;
    }
    const nextBlock = lastBlock.height + 1;
    let block = await this._getBlock(provider, nextBlock);
    // block.transactions = await this.getTransactionsByBlock(block);
    return block;
  }

  _getBlock(provider, blockHashOrBlockNumber, returnTransactionObjects = true) {
    // 3分钟还没同步到区块就自动退出
    const startTime = Date.now();
    const timer = setTimeout(() => {
      process.exit(0);
    }, 3 * 60 * 1000);

    return new Promise((resolve, reject) => {
      clearTimeout(timer);
      provider.eth.getBlock(blockHashOrBlockNumber, returnTransactionObjects, (err, data) => {
        if (err) {
          return resolve(null);
        }
        resolve(data);
      });
    });
  }
}

// const url = process.env.url;
new EthSyncer({
  providers: [
    // 'http://localhost:8545'
    'https://api.myetherapi.com/eth',
    url || 'https://mainnet.infura.io/qexcS4J1PiCKn9WlivBs'
  ]
});

/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('deposit_utxo', {
		txid: {
			type: DataTypes.STRING(255),
			allowNull: false,
			primaryKey: true
		},
		assets_type: {
			type: DataTypes.INTEGER(8),
			allowNull: false,
			primaryKey: true
		},
		output_no: {
			type: DataTypes.INTEGER(8),
			allowNull: false,
			primaryKey: true
		},
		address: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		value: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		status: {
			type: DataTypes.INTEGER(1),
			allowNull: false
		},
		create_time: {
			type: DataTypes.DATE,
			allowNull: false
		},
		update_time: {
			type: DataTypes.DATE,
			allowNull: false
		}
	}, {
		tableName: 'deposit_utxo',
		timestamps: false
	});
};

/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('cold_address', {
		address: {
			type: DataTypes.STRING(255),
			allowNull: false,
			primaryKey: true
		},
		type: {
			type: DataTypes.INTEGER(8),
			allowNull: false,
			primaryKey: true
		},
		signature: {
			type: DataTypes.STRING(1000),
			allowNull: false
		}
	}, {
		tableName: 'cold_address',
		timestamps: false
	});
};

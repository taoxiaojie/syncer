/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('transaction_fee', {
		txid: {
			type: DataTypes.STRING(255),
			allowNull: false,
			primaryKey: true
		},
		assets_type: {
			type: DataTypes.INTEGER(4),
			allowNull: false
		},
		status: {
			type: DataTypes.INTEGER(4),
			allowNull: false
		},
		create_date: {
			type: DataTypes.DATE,
			allowNull: false
		},
		fee: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'transaction_fee',
		timestamps: false
	});
};

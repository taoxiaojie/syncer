'use strict';
const moment = require('moment');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const sleep = require('ko-sleep');
const utils = require('../lib/utils');
const Op = Sequelize.Op;

const env = process.env;
class Utxo {
  constructor() {
    const dbconfig = {
      database: 'bdeals',
      username: 'root',
      port: env.db_port,
      password: env.db_password,
      host: env.db_host,
    };
    const sequelize = new Sequelize(dbconfig.database, dbconfig.username, dbconfig.password, {
      logging: false,
      //logging: console,
      host: dbconfig.host,
      port: dbconfig.port,
      dialect: 'mysql',
      pool: {
        max: 5,
        min: 0,
        idle: 10000,
      },
      timezone: '+08:00',
    });

    const models = {};
    const dir = path.join(__dirname);
    fs.readdirSync(dir).forEach(item => {
      const model = path.basename(item, '.js');
      if (model === 'index') {
        return;
      }
      const fn = require(path.join(dir, item));
      models[model] = fn(sequelize, Sequelize);
    });
    this.Utxo = models['utxo'];
    this.Address = models['withdraw_address'];
    this.DepositUtxo = models['deposit_utxo'];
  }

  async queryUtxo(assets_type, page, pageSize = 100) {
    const list = await this.Utxo.findAll({
      offset: (page - 1) * pageSize,
      limit: pageSize,
      where: {
        assets_type,
        status: [1, 2],
      },
      raw: true,
    });
    return list;
  }

  async addUtxo({txid, output_no, assets_type, address, status, value}) {
    assert(txid);
    assert(assets_type);
    assert(address);
    assert(value);
    const now = utils.getDateTime();

    const record = await this.Utxo.findOne({
      where: {
        txid,
        assets_type,
        output_no,
      },
      raw: true,
    });

    if (record) {
      if (record.status === 2 || record.status === 3 || record.status === status) {
        //已经被消费
        return;
      }
      return await this.Utxo.update({
        status,
        update_time: now,
      }, {
        where: {
          txid,
          assets_type,
          output_no,
        },
      });
    }

    return await this.Utxo.create({
      txid,
      status: parseInt(status) || 0,
      output_no,
      assets_type,
      address,
      value,
      create_time: now,
      update_time: now,
    });
  }

  async markDepositUtxoUsed({txid, output_no, assets_type, status}) {
    assert(txid);
    assert(assets_type);
    while (true) {
      try {
        return await this.DepositUtxo.update({
          status: status || 3,
          update_time: utils.getDateTime(),
        }, {
          where: {
            txid,
            assets_type,
            output_no,
            status: {
              // 已经使用的utxo不允许更新
              [Op.ne]: 3,
            }
          }
        });
      } catch (err) {
        console.log(err);
        console.log('markDepositUtxoUsed')
        return console.log({txid, output_no, assets_type, status});
        //await sleep('1s');
      }
    }
  }

  async addDepositUtxo({txid, output_no, assets_type, address, status, value}) {
    assert(txid);
    assert(assets_type);
    assert(address);
    const now = utils.getDateTime();
    while (true) {
      try {
        const record = await this.DepositUtxo.findOne({
          where: {
            txid,
            assets_type,
            output_no,
          },
          raw: true,
        });

        if (record) {
          if (record.status === 2 || record.status === 3 || record.status === status) {
            //已经被消费
            return;
          }
          return await this.DepositUtxo.update({
            status,
            update_time: now,
          }, {
            where: {
              txid,
              assets_type,
              output_no,
            },
          });
        }

        return await this.DepositUtxo.create({
          txid,
          status: parseInt(status) || 0,
          output_no,
          assets_type,
          address,
          value,
          create_time: now,
          update_time: now,
        });
      } catch (err) {
        console.log(err);
        console.log('addDepositUtxo');
        return console.log({txid, output_no, assets_type, address, status, value});
        //await sleep('1s');
      }
    }
  }

  async updateStatus(txid, output_no, assets_type, status) {
    await this.Utxo.update({
      status,
      update_time: utils.getDateTime(),
    }, {
      where: {
        txid,
        assets_type,
        output_no,
      }
    });
  }

  async getAddress(assets_type) {
    const list = await this.Address.findAll({
      where: {
        assets_type,
        status: 1
      },
      raw: true,
    });
    return list;
  }
}

module.exports = new Utxo;

/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('withdraw_address', {
		assets_type: {
			type: DataTypes.INTEGER(8),
			allowNull: false,
			primaryKey: true
		},
		address: {
			type: DataTypes.STRING(255),
			allowNull: false,
			primaryKey: true
		},
		secret: {
			type: DataTypes.STRING(1000),
			allowNull: false
		},
		status: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		},
		create_time: {
			type: DataTypes.DATE,
			allowNull: false
		},
		update_time: {
			type: DataTypes.DATE,
			allowNull: false
		}
	}, {
		tableName: 'withdraw_address',
		timestamps: false
	});
};

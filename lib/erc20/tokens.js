'use strict';
const tokens = [
  {
    name: 'TUSD',
    decimals: 18,
    address: '0x8dd5fbce2f6a956c3022ba3663759011dd51e73e',
  },
  {
    name: 'GUSD',
    decimals: 2,
    address: '0x056fd409e1d7a124bd7017459dfea2f387b6d5cd',
  },
  {
    name: 'OMG',
    decimals: 18,
    address: '0xd26114cd6ee289accf82350c8d8487fedb8a0c07',
  },
  {
    name: 'AE',
    decimals: 18,
    address: '0x5ca9a71b1d01849c0a95490cc00559717fcf0d1d',
  },
  {
    name: 'ETU',
    decimals: 0,
    address: '0x765b0aaef0ecac2bf23065f559e5ca3b81067993',
  }
  // {
  //   name: 'REP',
  //   decimals: 18, // 待定
  //   address: '0x2aabb1a69fa4506b08f82f9f1fd737f9ce1c5343',
  // }
];

const tokenMap = {};

for (let token of tokens) {
  tokenMap[token.address] = token;
}

module.exports = tokenMap;

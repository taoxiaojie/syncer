module.exports = (tableName, sequelize, Sequelize) => {
  return sequelize.define(tableName, {
    txid: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    block_hash: {
      type: Sequelize.STRING,
    },
    address: {
      type: Sequelize.STRING,
    },
    tx_index: {
      type: Sequelize.STRING,
    },
    value: {
      type: Sequelize.STRING,
    },
    script: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.INTEGER,
    }
  }, {
    indexes: [
      {
        name: 'idx_address',
        fields: ['address'],
      },
    ]
  });
}

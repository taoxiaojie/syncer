module.exports = (tableName, sequelize, Sequelize) => {
  return sequelize.define(tableName, {
    txid: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    block_hash: {
      type: Sequelize.STRING,
    },
    address: {
      type: Sequelize.STRING,
    },
    value: {
      type: Sequelize.STRING,
    },
  }, {
    indexes: [
      {
        name: 'idx_address',
        fields: ['address'],
      },
    ]
  });
}

'use strict';
const assert = require('assert');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.connect('mongodb://192.168.0.8:27018/bdeals', {useNewUrlParser:true});

const BaseSchema = {
  txid: { type: String, index: true }, // 交易id
  block_hash: {
    type: String,
    default: '',
  }, // 区块id
  block_height: {
    type: Number,
    default: 0,
  }, // 区块高度，默认0
  address: {
    type: String,
    index: true,
  }, // 充值地址
  value: String,// 充值金额
  status: {
    type: Number,
    default: 1,
  }, //状态1.进账中，2.已进账，3, 已打包出账 4.出账中，5.已出账
  create_time: {type: Date, default: Date.now},
  withdraw_time: {type: Date}, // 归集时间
  confirmed: {
    type: Number,
    default: 0, // 业务上是否已经确认充值成功 0 否， 1已确认
  }
};

// 用户的充值记录
const depositSchema = new Schema({
  ...BaseSchema,
  token: String,
});

const bitDepositSchema = new Schema({
  ...BaseSchema,
  output_no: Number, // 序号
});

// 平台热钱包(比特币系列)
const hotBitUtxoSchema = new Schema({
  ...BaseSchema,
  output_no: Number, // 序号
  token: String,
});

// 用户提现转账记录
const withdrawSchema = new Schema({
  txid: { type: String, index: true }, // 交易id
  block_hash: {
    type: String,
    default: '',
  }, // 区块id
  block_height: {
    type: Number,
    default: 0,
  }, // 区块高度，默认0
  address: {
    type: String,
    index: true,
  }, // 提现地址
  value: String,// 提现金额
  status: {
    type: Number,
    default: 1,
  }, //状态1.网络确认中，2已确认
  create_time: {type: Date, default: Date.now},
  confirmed: {
    type: Number,
    default: 0, // 业务上是否已经确认提现成功 0 否， 1已确认
  }
});

// 钱包账户
const accountSchema = new Schema({
  address: {
    type: String,
    index: true,
  },
  number: {
    type: Number, //地址编号
  },
  secret:String,
});

exports.mongoose = mongoose;

exports.getAccountModel = function(token) {
  assert(token, 'token is required');
  token = token.toLowerCase();
  const modelName = `${token}_deposit_accounts`;
  mongoose.model(modelName, accountSchema);
  return mongoose.model(modelName);
}

exports.getHotWalletModel = function() {
  const modelName = `hot_utxos`;
  mongoose.model(modelName, hotBitUtxoSchema);
  return mongoose.model(modelName);
}

exports.getBitDepositModel = function(tokenName) {
  assert(tokenName, 'token name is required');
  const modelName = `${tokenName}_deposits`;
  mongoose.model(modelName, bitDepositSchema);
  return mongoose.model(modelName);
}

exports.getDepositModel = function() {
  const modelName = 'deposits';
  mongoose.model(modelName, depositSchema);
  return mongoose.model(modelName);
}

exports.getWithdrawModel = function() {
  const modelName = 'withdraw';
  mongoose.model(modelName, withdrawSchema);
  return mongoose.model(modelName);
}

// Refer to document:  https://help.aliyun.com/document_detail/62670.html

const getPort = require('get-port');
const env = process.env;

module.exports = async function() {
  const port = await getPort();
  process.env.NODE_CLUSTER_CLIENT_PORT = port;
  const ACMClient = require('acm-client');
  return new ACMClient({
    endpoint: 'acm.aliyun.com', // Available in the ACM console
    namespace: '10dcb0d0-5475-48f3-8994-140f3bef79e3', // Available in the ACM console
    accessKey: env.acmAccessKey, // Available in the ACM console
    secretKey: env.acmSecretKey, // Available in the ACM console
    requestTimeout: 6000, // Request timeout, 6s by default
  });
}

'use strict';
const urllib = require('urllib');
const sleep = require('ko-sleep');
module.exports = async function(address) {
  let utxo = [];
  const pageSize = 100;
  while (true) {
    let txid = null;
    let url = `https://chain.so/api/v2/get_tx_unspent/BTC/${address}`;
    if (txid) {
      url= url + '/' + txid;
    }
    const res = await urllib.request(url, {
      dataType: 'json',
    });
    const {data, status} = res.data;
    if (status === 'success') {
      const txs = data.txs;
      for (let tx of txs) {
        utxo.push({
          address,
          assets_type: 4,
          txid: tx.txid,
          value: tx.value,
          output_no: tx.output_no,
        });
      }
      if (txs.length < pageSize) {
        return utxo;
      }
      await sleep('300ms');
    } else {
      return utxo;
    }
  }
}

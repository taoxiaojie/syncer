'use strict';
const urllib = require('urllib');
const sleep = require('ko-sleep');
const scale = 100000000;

module.exports = async function(address) {
  let utxo = [];
  let page = 1;
  while (true) {
    const res = await urllib.request(`https://bch-chain.api.btc.com/v3/address/${address}/unspent`, {
      dataType: 'json',
      data: {
        page,
      }
    });
    const data = res.data && res.data.data;
    if (data) {
      const list = data.list;
      for (let tx of list) {
        utxo.push({
          address,
          assets_type: 3,
          txid: tx.tx_hash,
          value: tx.value/scale,
          output_no: tx.tx_output_n,
        });
      }
      if (list.length < data.pagesize) {
        return utxo;
      }
      await sleep(500);
    } else {
      return utxo;
    }
  }
}

'use strict';
const bch = require('./bch');
const ltc = require('./ltc');
const btc = require('./btc');
module.exports = {
  bch,
  ltc,
  btc,
}

/**
* 比特币系钱包提现处理
*/
const timeout = 5 * 1000;
const scale = 100000000;
const fee = 0.0001 * scale;
const split = 20;
const BN = require('bignumber.js')
const BaseWithdraw = require('./withDraw');

module.exports = class BitwithDraw extends  BaseWithdraw {
  async markUsed(utxo) {
    const assets_type = this.type;
    const app = this.ctx.app;
    const Utxo = app.getModel('utxo');
    await Utxo.update({
      status: 3,
    }, {
      where: {
        txid: utxo.txId,
        output_no: utxo.outputIndex,
        assets_type,
      }
    });
  }

  async handle(list) {
    let utxo = await this.getUtxo();

    if (!(utxo && utxo.length)) {
      return;
    }

    let totalAmount = BN(0);
    const transform = [];
    for (let item of list) {
      if (item.txid || item.status !== 4) {
        continue;
      }

      const amount = parseFloat(item.amount);
      totalAmount =  totalAmount.plus(item.amount);
      transform.push({
        withdraw_id: item.id,
        address: item.address,
        satoshis: BN(item.amount).times(scale).toString(),
      });
    }

    if (!transform.length) {
      return;
    }

    const keys = await this.getAddress();

    utxo = utxo.filter(item => {
      return !!keys[item.address];
    });

    if (!utxo.length) {
      return;
    }

    const utxoSpend = [];
    let value = BN(0);
    for (let item of utxo) {
      value = value.plus(item.value);
      utxoSpend.push({
        "txId" : item.txid,
        "outputIndex" : item.output_no,
        "address" : item.address,
        "script" : this.getScript(item.address),
        "satoshis" : BN(item.value).times(scale).toNumber(),
      });

      if (value.gt(totalAmount.plus(tranformFee))) {
        break;
      }
    }

    const transformMap = {};
    let tranformFee = fee;

    for (let item of transform) {
      if (!transformMap[item.address]) {
        transformMap[item.address] = parseInt(item.satoshis);
        tranformFee = tranformFee + fee/split;
      } else {
        transformMap[item.address] = parseInt(item.satoshis) + transformMap[item.address];
      }
    }

    let transaction = this.getTransaction().from(utxoSpend).fee(tranformFee);

    const privateKeys = [];
    for (let address in transformMap) {
      transaction = transaction.to(address, transformMap[address]);
      privateKeys.push(this.getPrivateKey(keys[address]));
    }

    transaction = transaction.change(Object.keys(keys)[0]).sign(privateKeys);

    const rawtx = transaction.serialize();
    const hash = transaction.hash;

    for (let item of transform) {
      const res = await this.createTransaction({
        withdraw_id: item.withdraw_id,
        txid: hash,
      });
    }

    const res = await this.postSignedTransaction(rawtx);

    for (let item of utxoSpend) {
      await this.markUsed(item);
    }

    await this.sleep('5s');
  }
}

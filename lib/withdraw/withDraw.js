/**
* 定时处理用户提现
*/
'use strict';
const Subscription = require('egg').Subscription;
const sleep = require('ko-sleep');
const size = 30; // 一次处理转账笔
const assert = require('assert');
const address = Symbol('#address');

module.exports =  class WithDraw extends Subscription {
  // 通过 schedule 属性来设置定时任务的执行间隔等配置
  static get schedule() {
    return {
      interval: '5m', //
      type: 'worker', //
      immediate: true,
    };
  }

  decrypt(text) {
    return this.ctx.app.decrypt(text);
  }

  async _getAddress(assets_type) {
    const app = this.ctx.app;
    const Address = app.getModel('withdraw_address');
    const data = {};
    const list = await Address.findAll({
      where: {
        status: 1,
        assets_type,
      },
      raw: true,
    });
    list.map(item => {
      data[item.address] = this.decrypt(item.secret);
    });
    return data;
  }

  async getAddress() {
    const assets_type = this.type;
    if (!this[address]) {
      this[address] = await this._getAddress(assets_type);
      setInterval( async () => {
        this[address] = await this._getAddress(assets_type);
      }, 1000 * 60);
    }
    return this[address];
  }

  async getUtxo() {
    const assets_type = this.type;
    const app = this.ctx.app;
    const Utxo = app.getModel('utxo');
    let list = [];
    let page = 1;
    const pageSize = 500;
    while (true) {
      const data = await Utxo.findAll({
        offset: (page++ - 1) * pageSize,
        limit: pageSize,
        where: {
          assets_type,
          status: 1,
        },
        raw: true,
      });
      list = list.concat(data);
      if (data.length < pageSize) {
        return list;
      }
    }
  }

  get type() {
    throw new Error('not implemented');
  }

  async sleep(ms) {
    await sleep(ms);
  }

  async handle(list) {
    console.log(list);
  }

  async confirmTransaction({block, txid}) {
    assert(block, 'block is required');
    assert(txid, 'txid is required');
    const assets_type = this.type;
    while (true) {
      try {
        const res = await this.ctx.callApi('withdraw.confirmTransaction', {
          block, txid, assets_type
        });
        return;
      } catch (err) {
        await sleep('3s');
      }
    }
  }

  async createTransaction({withdraw_id, txid}) {
    assert(withdraw_id, 'withdraw_id is required');
    assert(txid, 'txid is required');
    while (true) {
      try {
        return await this.ctx.callApi('withdraw.createTransaction', {
          withdraw_id,
          txid,
        });
      } catch (err) {
        await sleep('3s');
      }
    }
  }

  async getLock(key, timeout = 10 * 1000) {
    key = `lock:${this.type}:${key}`;
    return await this.ctx.app.getLock(key, parseInt(timeout/1000));
  }

  async releaseLock(key) {
    key = `lock:${this.type}:${key}`;
    return await this.ctx.app.releaseLock(key);
  }

  // subscribe 是真正定时任务执行时被运行的函数
  async subscribe() {
    const app = this.ctx.app;
    if (app.config.env === 'test') {
      return;
    }

    // 自动转账开关
    if (parseInt(app.autowithdraw) !== 1) {
      return;
    }
    let page = 1;

    while (true) {
      const res = await this.ctx.callApi('withdraw.query', {
        status: 4,
        assets_type: this.type,
        page,
        pageSize: size,
      });

      if (!(res && res.success)) {
        await sleep('5s');
        continue;
      }
      const data = res.data;
      const {count, list} = data;
      try {
        await this.handle(list);
      } catch (err) {
        console.log(err);
      }
      if (count < size) {
        return;
      }
      await sleep('5s');
    }
  }
}

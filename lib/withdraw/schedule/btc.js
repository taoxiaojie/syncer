/**
* 定时处理用户提现
*/
'use strict';
const WithDraw = require('../lib/bitWithdraw');
const BN = require('bignumber.js')
const notify = require('../lib/notify');
const urllib = require('urllib');
const bitcore = require('bitcore-lib');
const { Script, Address, PublicKey, PrivateKey, Transaction} = bitcore;
const timeout = 5 * 1000;
const scale = 100000000;
const fee = 0.0001 * scale;

module.exports = class BtcWithDraw extends WithDraw {
  get type() {
    return 1;
  }

  getPrivateKey(key) {
    return new PrivateKey(key);
  }

  getScript(address) {
    return new Script.fromAddress(new Address(address));
  }

  getTransaction() {
    return new Transaction();
  }
}

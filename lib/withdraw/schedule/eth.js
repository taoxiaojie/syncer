/**
* 定时处理用户提现
*/
'use strict';
const WithDraw = require('../lib/withDraw');
const EthereumTx = require('ethereumjs-tx')
const Web3 = require('web3');
const BN = require('bignumber.js')
const notify = require('../lib/notify');
const web3 = new Web3('https://mainnet.infura.io/qexcS4J1PiCKn9WlivBs');
const gasPrice = web3.utils.toWei('0.000000006', 'ether');
const gasLimit = 21000;
const gas = BN(gasPrice).times(gasLimit).toString();
const timeout = 5 * 1000;

class EthWithDraw extends WithDraw {
  get type() {
    return 2;
  }

  async getWallet() {
    if (!this.ctx.app.withdrawInfo) {
      return null;
    }
    const list = this.ctx.app.withdrawInfo['eth'];
    const keys = Object.keys(list);
    while (true) {
      for (let address of keys) {
        const lock = await this.getLock(address, timeout);
        if (lock) {
          return {
            address,
            key: list[address]
          }
        }
      }
      await this.sleep(timeout);
    }
  }

  async handle(list) {
    return;
    const wallet = await this.getWallet();
    if (!(wallet && wallet.address)) {
      return;
    }
    const {address} = wallet;
    let count = await web3.eth.getTransactionCount(address);
    let balance = await web3.eth.getBalance(address);
    let left = BN(balance);
    const data = [];
    for (let item of list) {
      if (item.status === 2) {
        const {update_time, txid} = item;
        const tx = await web3.eth.getTransaction(txid);
        if (tx) {
          if (tx.blockHash) {
            await this.confirmTransaction({
              block: tx.blockHash,
              txid,
            });
          }
          continue;
        } else if (Date.now() - new Date(update_time) < 15 * 1000){
          continue;
        }
      }

      if (item.txid) {
        continue;
      }
      const amount = web3.utils.toWei(item.amount, 'ether');
      left = left.minus(item.amount).minus(amount).minus(gas);
      const tx =  this.getTransaction({
        key: wallet.key,
        address: item.address,
        amount,
      }, count++);
      tx.withdraw_id = item.id;
      data.push(tx);
    }

    if (left.lt(0)) {
      notify(`eth钱包${address},余额不足(还差${left.toString()}eth)，处理提现失败，请尽快充值`);
      await this.sleep(timeout);
      return;
    }

    const res = [];
    for (let item of data) {
      const ret = await this.createTransaction({
        withdraw_id: item.withdraw_id,
        txid: item.hash,
      });
      res.push(ret.data);
    }

    if (!res.length) {
      return;
    }
    const result = await Promise.all(data.map(tx => {
      return web3.eth.sendSignedTransaction(tx.serializedTx);
    }));
    for (let item of result) {
      await this.confirmTransaction({
        block: item.blockHash,
        txid: item.transactionHash,
      });
    }
    await this.sleep('5s');
  }

  getTransaction(item, count) {
    const txParams = {
      nonce: count,
      gasPrice: web3.utils.toHex(gasPrice),
      gasLimit,
      to: item.address,
      value: web3.utils.toHex(item.amount),
      data: '0x',
      chainId: 1
    };

    const tx = new EthereumTx(txParams);
    tx.sign(Buffer.from(item.key.replace(/^0x/, ''), 'hex'));
    const hash = `0x${tx.hash().toString('hex')}`;
    let serializedTx = tx.serialize().toString('hex');
    if (!serializedTx.startsWith('0x')) {
      serializedTx = '0x' + serializedTx;
    }

    return {
      ...txParams,
      serializedTx,
      hash,
    }
  }
}

module.exports = EthWithDraw;

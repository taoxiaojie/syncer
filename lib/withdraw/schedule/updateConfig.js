/**
* 定时向地址池子中添加地址
*/
'use strict';
const Subscription = require('egg').Subscription;
const size = 10000; // 一次注入1万个地址

class Address extends Subscription {
  static get schedule() {
    return {
      interval: '10s', //
      type: 'worker', //
      immediate: true,
    };
  }

  async subscribe() {
    const app = this.ctx.app;
    if (app.config.env === 'test') {
      return;
    }
    const ret = await this.ctx.callApi('system.getConfig', {
      dataId: 'bdeals_admin',
    });
    const config = JSON.parse(ret.data);
    Object.assign(app, config);

    const ret1 = await this.ctx.callApi('system.getConfig', {
      dataId: 'withdraw',
    });

    const data = JSON.parse(ret1.data);
    const info = {};
    for (let key in data) {
      const items = data[key];
      const address = {};
      for (let item of items) {
        const temp = item.split(':');
        address[temp[0]] = app.decrypt(temp[1]);
      }
      info[key] = address;
    }
    app.withdrawInfo = info;
  }
}

module.exports = Address;

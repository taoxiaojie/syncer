/**
* 定时处理用户提现
*/
'use strict';
const WithDraw = require('../lib/bitWithdraw');
const BN = require('bignumber.js');
const notify = require('../lib/notify');
const urllib = require('urllib');
const litecore = require('litecore-lib');
const { Script, Address, PublicKey, PrivateKey, Transaction} = litecore;
const timeout = 5 * 1000;
const scale = 100000000;
const fee = 0.0001 * scale;

module.exports = class LtcWithDraw extends WithDraw {
  get type() {
    return 4;
  }

  getPrivateKey(key) {
    return new PrivateKey(key);
  }

  getScript(address) {
    return new Script.fromAddress(new Address(address));
  }

  getTransaction() {
    return new Transaction();
  }

  async postSignedTransaction(rawtx) {
    while (true) {
      const res = await urllib.request('https://chain.so/api/v2/send_tx/LTC', {
        method: 'post',
        dataType: 'json',
        data: {
          tx_hex: rawtx,
        }
      });
      const data = res.data;
      if (data.txid) {
        return data;
      }
      // 1s后重试
      await this.sleep('1s');
    }
  }

}

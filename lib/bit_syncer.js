/**
* bitcoin 系处理
*/
const urllib = require('urllib');
const utxo = require('../models/index');
const BaseSyncer = require('./base_syncer');
const debug = require('debug');
const path = require('path');
const fs = require('fs');

module.exports = class BitSyncer extends BaseSyncer {
  constructor(options) {
    super(options);
    this.utxo = utxo;
    this.withdrawAddress = {};
    this.startListenUtxo();
  }

  async addUtxo({txid, address, output_no, status, value}) {
    if (!this.withdrawAddress[address]) {
      return;
    }
    await this.utxo.addUtxo({
      value,
      txid,
      address,
      output_no,
      status,
      assets_type: this.assets_type,
    });
  }

  async hasUtxo(txid, output_no) {
    const key = `utxo:${txid}:${output_no}`;
    try {
      const ret = await this.redis.get(key);
      return !!ret;
    } catch (err) {
      return false;
    }
  }

  isWatched(address) {
    return this.withdrawAddress[address];
  }

  // 监听提现地址的utxo变化
  startListenUtxo() {
    const assets_type = this.assets_type;
    setTimeout( async()=> {
      while (true) {
        const address = await this.utxo.getAddress(assets_type);
        const data = {};
        for (let item of address) {
          data[item.address] = true;
        }
        this.withdrawAddress = data;
        await this.sleep('60s');
      }
    }, 0);

    setTimeout(async () => {
      let page = 1;
      const pageSize = 100;
      while (true) {
        const list = await this.utxo.queryUtxo(assets_type, page++, pageSize);
        for (let item of list) {
          const key = `utxo:${item.txid}:${item.output_no}`;
          if (this.isSpent) {
            const is_spent = await this.isSpent(item.txid, item.output_no);
            await this.sleep('3s');
            if (is_spent) {
              await this.markUsed(txid, output_no);
              continue;
            }
          }
          await this.redis.set(key, item.status, 'ex', 24 * 60 * 60);
        }
        if (list.length < pageSize) {
          page = 1;
          await this.sleep('60s');
        } else {
          await this.sleep('3s');
        }
      }
    }, 0);
  }

  async markUsed(txid, output_no) {
    await this.utxo.updateStatus(txid, output_no, this.assets_type, 3);
  }

  // 同步未确认的区块
  async syncPenddingTransactions() {
    while (true) {
      await this.sleep('5s');
      try {
        const data = await this._request('/mempool/contents.json');
        for (let txid in data) {
          const tx = await this.getTx(txid);
          await this.sleep(100);
          this.handleTx(tx, 0);
        }
      } catch (err) {
        console.log(err);
      }
    }
  }

  async _request(api) {
    if (api.indexOf('/') === 0) {
      api = api.substring(1);
    }
    const url = this.baseUrl + api;
    const res = await urllib.request(url, {
      timeout: '10s',
      dataType: 'json',
    });
    return res.data;
  }

  async getLatestBlock() {
    const info = await this._request('chaininfo.json');
    const bestblockhash = info.bestblockhash;
    return await this.getBlockByHash(bestblockhash);
  }

  getAddress(address, type) {
    return address;
  }

  async removeUtxo(txid, vout, height) {
    if (!txid) {
      return;
    }

    const has = await this.hasUtxo(txid, vout);

    if (has) {
      await this.markUsed(txid, vout);
      await this.utxo.markDepositUtxoUsed({
        txid,
        assets_type: this.assets_type,
        output_no: vout,
        status: height ? 3 : 2,
      });
    }

    const txfile = path.join(this.utxoTxDir, txid+'_'+vout);
    if (!fs.existsSync(txfile)) {
      return;
    }

    const data = JSON.parse(fs.readFileSync(txfile));
    const address = data.address;
    const addressFile = path.join(this.utxoAddressDir, address);
    if (!fs.existsSync(addressFile)) {
      try {
        fs.unlinkSync(txfile);
      } catch(err) {

      }
      return;
    }
    const txes = JSON.parse(fs.readFileSync(addressFile));
    delete txes[txid];
    if (Object.keys(txes).length > 0) {
      fs.writeFileSync(addressFile, JSON.stringify(txes));
    } else {
      try {
        fs.unlinkSync(addressFile);
      } catch(err) {

      }
    }
  }

  handleTx(transaction, height, blockHash) {
    const assets_type = this.assets_type;
    const {vout, vin,  txid} = transaction;
    this.debug(`got a transaction %j`, transaction);
    for (let item of vin) {
      if (item.txid) {
        setTimeout(async () => {
          await this.removeUtxo(item.txid, item.vout, height);
        });
      }
    }

    for (let out of vout) {
      const {scriptPubKey, value, n} = out;
      const amount = this.formatAmount(value);
      if (parseFloat(amount) < this.minAmount) {
        continue;
      }
      const addresses = scriptPubKey && scriptPubKey.addresses;
      if (!(addresses && addresses.length)) {
        continue;
      }

      for (let address of addresses) {
        try {
          if (scriptPubKey.type === 'pubkey') {
            continue;
          }

          address = this.getAddress(address, scriptPubKey.type);

          const data = {
            height,
            txid,
            block: blockHash,
            amount,
            address,
            tx_index: n,
            script: scriptPubKey.hex,
          };

          this.debug('transaction %j', data);
          if (height) {
            this.addUtxo({
              value: amount,
              txid,
              address,
              output_no: n,
              status: 1,
            });
          }

          this.postTransaction(data);
          setTimeout(async () => {
            const has = await this.addressManager.has(address);
            if (!has) {
              return;
            }
            await this.utxo.addDepositUtxo({
              txid,
              status: height ? 1 : 0,
              output_no: n,
              assets_type,
              value: amount,
              address,
            });
            const key = `utxo:${txid}:${n}`;
            await this.redis.set(key, 1, 'EX', 7 * 24 * 60 * 60);
          });
        } catch (err) {
          console.log(err);
        }
      }
    }
  }

  async getTx(txid) {
    return await this._request(`/tx/${txid}.json`);
  }

  getBchAddress(address, type) {
    if (address.indexOf('bitcoincash:') === 0) {
      address = bchaddr.toLegacyAddress(address);
    }
    return address.toString();
  }

  async getBlock(lastBlock) {
    let hash = lastBlock.nextblockhash;
    const height = lastBlock.height;
    if (!hash) {
      hash = await this.client.getBlockHash(height + 1);
    }
    return await this.getBlockByHash(hash);
  }

  async getTransactionByHash(txid) {
    const data = await this.getTx(txid);
    if (data) {
      return {
        ...data,
        hash: data.blockhash,
      };
    }
  }

  async getBlockByHash(hash) {
    if (!hash) {
      return;
    }
    const block = await this._request(`/block/${hash}.json`);
    return block;
  }
}

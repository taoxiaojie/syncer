'use strict';
const path = require('path');
const Sequelize = require('sequelize');
const TXIN = require('./model/tx_in');
const TXOUT = require('./model/tx_out');

module.exports = class DataBase {
  constructor(name) {
    this.assetsName = name;
    const env = process.env;
    const {database, mysql_user, mysql_password, host } = env;
    console.log(`database is `, database);
    // if (database) {
    //   this.db  = new Sequelize(database, mysql_user, mysql_password, {
    //     host: host || 'localhost',
    //     dialect: 'mysql',
    //     pool: {
    //       max: 5,
    //       min: 0,
    //       acquire: 30000,
    //       idle: 10000
    //     },
    //   });
    //   this.init();
    // }
  }

  async init() {
    await this.db.authenticate();
    this.TxIn = TXIN(this.assetsName+'_txin', this.db, Sequelize);
    this.TxOut = TXOUT(this.assetsName+'_txout', this.db, Sequelize);
    await this.TxIn.sync();
    await this.TxOut.sync();
  }

  async insertTxout(tx) {
    const model = this.TxOut;
    if (!model) {
      return;
    }
  }

  async insert(tx) {
    const model = this.TxIn;
    if (!model) {
      return;
    }
    try {
      await model.create(tx);
    } catch (err) {
      // console.log(err);
    }
  }
}

/**
* 生成充值地址
* bch和usdt 使用和btc一样的地址充值
* erc20 代币使用和eth一样的地址
*/
'use strict';
const bitcore = require('bitcore-lib');
const litecore = require('litecore-lib');
const WalletLib = require('ethereumjs-wallet');
const utils = require('web3-utils');
const assert = require('assert');
const xpub = 'xpub6CyxToyiN9fSxr8T3f54Mxuwxr8o5anGAqDmxR9eaM9N4q1sxBh2UWxtPuqDhcSbPTpjBsvnuJiM3EtUvModZq1tHxg6oQWYoUCVL5S6SSD';
const root = new bitcore.HDPublicKey(xpub);

const pub1 = root.derive(1024);
const pub2 = root.derive(1025);
let start = Math.pow(2, 30);

function getBTCAddress(position) {
  const node1 = pub1.derive(position);
  const node2 = pub2.derive(position);
  const publicKeys = [
    node1.publicKey.toString(),
    node2.publicKey.toString(),
  ];
  return (new bitcore.Address(publicKeys, 2)).toString();
}

function getETHAddress(position) {
  const node = pub1.derive(position);
  return utils.toChecksumAddress(WalletLib.fromExtendedPublicKey(node.toString()).getAddressString());
}

function getLTCAddress(position) {
  const node = pub1.derive(position);
  const publicKey = node.publicKey.toString();
  return new litecore.PublicKey(publicKey).toAddress().toString();
}

exports.getAddress = function(assets_type, position) {
  position = position + start;
  if (assets_type === 1) {
    return getBTCAddress(position);
  }

  if (assets_type === 2) {
    return getETHAddress(position);
  }

  if (assets_type === 4) {
    return getLTCAddress(position);
  }
}

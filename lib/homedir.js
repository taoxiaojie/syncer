'use strict';
const os = require('os');
module.exports = process.env.homedir || os.homedir();

'use strict';
const urllib = require('urllib');
const assert = require('assert');
const sleep = require('ko-sleep');
const host = 'https://b.deals/api';
const NodeRSA = require('node-rsa');
const CryptoJS = require("crypto-js");
const debug = require('debug')('api');
const LRU = require("lru-cache");
const options = {
  max: 100,
  maxAge: 1000 * 10 * 60,
};
const cache = LRU(options);

const key = new NodeRSA(Buffer.from(process.env.privateKey, 'hex').toString());

function  postPendingTransaction({
  assets_type, txid, amount, address
}) {
  assert(assets_type, 'assets_type is required');
  assert(txid, 'txid is required');
  assert(amount, 'amount is required');
  assert(address, 'address is required');

  if (cache.get(txid)) {
    return;
  }
  const random = Math.random().toString();
  const data = {
    assets_type,
    txid,
    amount,
    address,
    random,
  };
  const raw = key.encrypt(JSON.stringify(data), 'base64');
  debug('postPendingTransaction', data);
  return urllib.request(`${host}/createTransaction`, {
    method: 'post',
    data: {
      raw,
    },
  }).then(res => {
    if (res.status === 200) {
      cache.set(txid, 1);
      const bytes  = CryptoJS.AES.decrypt(res.data.toString(), random);
      const data = bytes.toString(CryptoJS.enc.Utf8);
      debug('postPendingTransaction result', data);
      return data;
    }
    throw new Error(res.data.toString());
  });
}

function postConfirmWithdraw({ block, txid, assets_type }) {
  assert(assets_type, 'assets_type is required');
  assert(txid, 'txid is required');
  assert(block, 'block is required');

  const key = `withdraw:${txid}`;
  if (cache.get(key)) {
    return;
  }
  const random = Math.random().toString();
  const data = {
    assets_type,
    txid,
    block,
    random,
  };
  const raw = key.encrypt(JSON.stringify(data), 'base64');
  debug('confirmWithdraw', data);

  return urllib.request(`${host}/confirmWithdraw`, {
    method: 'post',
    data: {
      raw,
    },
  }).then(res => {
    if (res.status === 200) {
      cache.set(key, 2);
      const bytes  = CryptoJS.AES.decrypt(res.data.toString(), random);
      const data = bytes.toString(CryptoJS.enc.Utf8);
      debug('confirmWithdraw result', data);
      return data;
    }
    throw new Error(res.data.toString());
  });
}

function  postConfirmedTransaction({
  assets_type, txid, block,
}) {
  assert(assets_type, 'assets_type is required');
  assert(txid, 'txid is required');
  assert(block, 'block is required');

  if (cache.get(txid) === 2) {
    return;
  }
  const random = Math.random().toString();
  const data = {
    assets_type,
    txid,
    block,
    random,
  };
  const raw = key.encrypt(JSON.stringify(data), 'base64');
  debug('postConfirmedTransaction', data);

  return urllib.request(`${host}/confirmTransaction`, {
    method: 'post',
    data: {
      raw,
    },
  }).then(res => {
    if (res.status === 200) {
      cache.set(txid, 2);
      const bytes  = CryptoJS.AES.decrypt(res.data.toString(), random);
      const data = bytes.toString(CryptoJS.enc.Utf8);
      debug('postConfirmedTransaction result', data);
      return data;
    }
    throw new Error(res.data.toString());
  });
}

exports.postPendingTransaction = async function(data) {
  while (true) {
    try {
      const res = await postPendingTransaction(data);
      return
    } catch(err) {
      console.log(data);
      console.log("postPendingTransaction error", err);
      await sleep('5s');
    }
  }
}

exports.postConfirmedTransaction = async function(data) {
  while (true) {
    try {
      await postConfirmedTransaction(data);
      return;
    } catch(err) {
      console.log(err);
      await sleep('5s');
    }
  }
}

exports.confirmWithdraw = async function(data) {
  while (true) {
    try {
      await postConfirmWithdraw(data);
      return;
    } catch(err) {
      console.log(err);
      await sleep('5s');
    }
  }
}

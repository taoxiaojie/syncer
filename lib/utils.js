'use strict';
const md5 = require('md5');
const moment = require('moment');
const format = 'YYYY-MM-DD HH:mm:ss';
const is = require('is-type-of');
exports.md5 = md5;

exports.assignIfDefined = function(target={}, source={}, keys) {
  if (is.string(keys)) {
    keys = [keys];
  }
  for (let key of keys) {
    if (!is.nullOrUndefined(source[key])) {
      target[key] = source[key];
    }
  }
  return target;
}

exports.getDateTime = function(date) {
  if (date && date.format) {
    return date.format(format);
  }
  if (/^\d+$/.test(date)) {
    return moment(new Date(parseInt(date))).format(format);
  }

  if (is.date(date)) {
    return moment(date).format(format);
  }
  return moment().format(format);
};

exports.PKCS8 = 'pkcs#8';
exports.PKCS1 = 'pkcs#1';

const PUBDESC = {
  'pkcs#8': {
    begin: '-----BEGIN PUBLIC KEY-----',
    end: '-----END PUBLIC KEY-----',
  },
  'pkcs#1': {
    begin: '-----BEGIN PRIVATE KEY-----',
    end: '-----END PRIVATE KEY-----',
  },
};

const PRIDESC = {
  'pkcs#8': {
    begin: '-----BEGIN PRIVATE KEY-----',
    end: '-----END PRIVATE KEY-----',
  },
  'pkcs#1': {
    begin: '-----BEGIN PRIVATE KEY-----',
    end: '-----END PRIVATE KEY-----',
  },
};

const isPem = function(key) {
  return key.startsWith('-----BEGIN');
};

exports.pemPubKey = function(key, type) {
  if (isPem(key)) {
    return key;
  }
  const desc = PUBDESC[type || exports.PKCS8];
  return transfer(key, desc.begin, desc.end);
};

exports.pemPriKey = function(key, type) {
  if (isPem(key)) {
    return key;
  }
  const desc = PRIDESC[type || exports.PKCS1];
  return transfer(key, desc.begin, desc.end);
};

// 转换为 PEM 格式
function transfer(context, begin, end) {
  const count = 64;
  const arr = [ begin ];
  const line = parseInt((context.length - 1) / count) + 1;
  for (let i = 0; i < line; i++) {
    const str = context.substring(count * i, count * (i + 1));
    arr.push(str);
  }
  arr.push(end);
  return arr.join('\n');
}

exports.toBuffer = function(data, charset) {
  if (!Buffer.isBuffer(data)) {
    return new Buffer(data, charset);
  }
  return data;
};

// 连接数据
exports.concat = function(a, b, type) {
  if (!type) {
    return Buffer.concat([ a, b ]);
  }
  return a + b;

};

const BASE64_ESCAPE_CHAR = '_';
/*
 * 将 '_' + 小写字母 转换成 大写字母。
 */
exports.base64DecodeUpperCase = function(str) {
  const len = str.length;
  let result = '';
  for (let i = 0; i < str.length; i++) {
    const ch = str[i];
    try {
      if (ch === BASE64_ESCAPE_CHAR && i < len - 1) {
        const nextChar = str[++i].toUpperCase();
        result += nextChar;
      } else {
        result += ch;
      }
    } catch (err) {
      // nothing
    }
  }
  return result;
};

exports.base64EncodeUpperCase = function(base64Src) {
  if (Buffer.isBuffer(base64Src)) {
    base64Src = base64Src.toString('base64');
  }
  return base64Src.replace(/[A-Z]{1}/g, function(ch) {
    return BASE64_ESCAPE_CHAR + ch.toLowerCase();
  });
};

exports.arraycopy = function(src, srcPos, dest, destPos, length) {
  for (let i = srcPos; i < srcPos + length; i++) {
    dest[destPos++] = src[i];
  }
};

const START_TAG = 0x30;
const SEP_TAG = 0x02;


exports.signatureToDNetRS = function(signature) {
  if (!Buffer.isBuffer(signature) || signature[0] !== START_TAG) {
    throw new Error('Invalid signature format');
  }

  const rs = new Buffer(40);
  rs.fill(0x00);

  let offsetR = 4;
  let lengthR = signature[offsetR - 1];
  let startR = 0;

  if (signature[offsetR - 2] !== SEP_TAG) {
    throw new Error('Invalid signature format');
  }

  if (lengthR > 20) {
    offsetR += lengthR - 20;
    lengthR = 20;
  } else if (lengthR < 20) {
    startR += 20 - lengthR;
  }

  let offsetS = signature[3] + 6;
  let lengthS = signature[offsetS - 1];
  let startS = 20;

  if (signature[offsetS - 2] !== SEP_TAG) {
    throw new Error('Invalid signature format');
  }

  if (lengthS > 20) {
    offsetS += lengthS - 20;
    lengthS = 20;
  } else if (lengthS < 20) {
    startS += 20 - lengthS;
  }

  exports.arraycopy(signature, offsetR, rs, startR, lengthR);
  exports.arraycopy(signature, offsetS, rs, startS, lengthS);

  return rs;
};

// 将加密内容以 8 的倍数以 0 补全
exports.padZeroBuf = function(buf) {
  const len = buf.length;
  const paddingLen = Math.ceil(len / 8) * 8;
  const less = paddingLen - len;
  const padding = new Buffer(less).fill(0);
  return Buffer.concat([ buf, padding ]);
};

// 去除末尾的 0 buffer
exports.delZeroBuf = function(buf) {
  for (let i = buf.length - 1; i >= 0; i--) {
    if (buf !== 0) {
      buf = buf.slice(0, i);
      break;
    }
  }
  return buf;
};


exports.stripDecimal = function stripDecimal(number) {
  if (number === 0 || number === '0') {
    return '0';
  }

  if (!number) {
    return number;
  }
  number = number.toString();
  if (number.indexOf('.') === -1) {
    return number;
  }
  number = number.replace(/0+$/g, '');
  if (number.slice(-1) === '.') {
    return number.slice(0, -1);
  }
  return number;
}

exports.formatAmount = function (amount, n = 8) {
  if (!amount) {
    return 0;
  }
  amount = amount.toString();
  amount = exports.stripDecimal(amount);
  const index = amount.indexOf('.');
  if (index > -1) {
    return amount.substring(0, index + n + 1);
  }
  return amount;
}

exports.encodePhone = function(phone) {
  return phone.toString().replace(/(\d{3})\d+(\d{3})/, '$1*****$2')
}

exports.getOpenId = function(uid) {
  return md5([uid, uid, uid].join('b.deals rocks'));
}

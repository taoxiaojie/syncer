// 调用bdeals 充值与提现接口
'use strict';
const sleep = require('ko-sleep');
const urllib = require('urllib');
const CryptoJS = require("crypto-js");
const rsa = new NodeRSA(Buffer.from(process.env.privateKey, 'hex').toString());

module.exports = class API {
  constructor() {
    const host = process.env.apiHost || 'https://b.deals';
    let apiUrl;
    if (host.endsWith('/')) {
      apiUrl = host + 'deals/api.json';
    } else {
      apiUrl = host + '/deals/api.json';
    }
    this.apiUrl = apiUrl;
  }

  call(api, params, method='GET') {
    const random = Math.random().toString();
    const data = {
      api,
      params,
      random,
    };
    const raw = rsa.encrypt(JSON.stringify(data), 'base64');
    return urllib.request(this.apiUrl, {
      timeout: '10s',
      method,
      data: {
        data: raw,
      },
      dataType: 'json',
    }).then(res => {
      if (res.status === 200) {
        return res.data;
      }
    });
  }

  async confirmWithdraw({block, txid, assets_type}) {
    assert(block, 'block is required');
    assert(txid, 'txid is required');
    assert(assets_type, 'assets_type is required');

    while (true) {
      try {
        const res = await this.call('withdraw.confirmTransaction', {
          block, txid, assets_type,
        });
        return res.data;
      } catch (err) {
        console.log(err);
        await sleep('3s');
      }
    }
  }

  async queryWithdraw({assets_type, page, pageSize}) {
    const res = await this.call('withdraw.query', {
      status: 4,
      assets_type,
      page,
      pageSize,
    });
    const data = res.data;
    return data;
  }

  async createWithdrawTransaction({withdraw_id, txid}) {
    assert(withdraw_id, 'withdraw_id is required');
    assert(txid, 'txid is required');
    while (true) {
      try {
        return await this.call('withdraw.createTransaction', {
          withdraw_id,
          txid,
        });
      } catch (err) {
        console.log(err);
        await sleep('3s');
      }
    }
  }
  // 处理提现
  async checkWithdraw(assets_type) {
    let page = 1;
    const pageSize = 50;
    while (true) {
      const res = await this.call('withdraw.query', {
        status: [2],
        assets_type,
        page: page++,
        pageSize,
      });

      if (!(res && res.success)) {
        await sleep('5s');
        continue;
      }
      const data = res.data;
      const {count, list} = data;
      try {
        // await this.handle(list);
      } catch (err) {
        console.log(err);
      }
      if (count < pageSize) {
        page = 1;
      }
      await sleep('5s');
    }
  }
}

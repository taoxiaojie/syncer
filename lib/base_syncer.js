'use strict';
const sleep = require('ko-sleep');
const path = require('path');
const mkdirp = require('mkdirp');
const fs = require('fs');
const os = require('os');
const api = require('../lib/api');
const acm = require('../lib/acm');
const ms = require('ms');
const debug = require('debug');
const Base = require('sdk-base');
const AddressManager = require('../lib/address');
const DataBase = require('./database');
const homedir = require('./homedir');
const host = require('./host');
const utxo = require('../models/index');
const NodeRSA = require('node-rsa');
const urllib = require('urllib');
const CryptoJS = require("crypto-js");
const rsa = new NodeRSA(Buffer.from(process.env.privateKey, 'hex').toString());
const assert = require('assert');
const dataId = 'withdraw';
const group = 'DEFAULT_GROUP';

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.stack);
});

module.exports = class BaseSyncer extends Base {
  constructor(options={}) {
    super(options);
    this.debug = debug(this.assetsName);
    this.host = host;
    this.options = options;
    const assetsName = this.assetsName;
    this.addressManager = new AddressManager(options.addressName || assetsName);
    this.redis = this.addressManager.redis;
    const dir = path.join(homedir, `.${assetsName}`);
    mkdirp.sync(dir);
    const txdir = path.join(dir, 'tx');
    mkdirp.sync(txdir);
    this.txdir = txdir;
    const utxodir = path.join(dir, 'utxo');
    this.utxoAddressDir = path.join(utxodir, 'address');
    this.utxoTxDir = path.join(utxodir, 'tx');
    mkdirp.sync(this.utxoAddressDir);
    mkdirp.sync(this.utxoTxDir);

    this._initWaitTime = false;
    this.progressFile = path.join(dir, 'progress.txt');
    this.waitTime = ms('10s'); // 初始值设为10s，后面根据区块时间动态调整
    this.checkWithdraw();
    this.init();
  }

  async handle(list) {
    for (let item of list) {
      const {txid} = item;
      if (this.getTransactionByHash) {
        try {
          const transaction = await this.getTransactionByHash(txid);
          if (transaction && transaction.hash) {
            await this.confirmTransaction({
              block: transaction.hash,
              txid,
            });
          }
        } catch (err) {
          continue;
        }
      }
    }
  }

  async confirmTransaction({block, txid}) {
    assert(block, 'block is required');
    assert(txid, 'txid is required');
    const assets_type = this.assets_type;
    while (true) {
      try {
        const res = await this.callApi('withdraw.confirmTransaction', {
          block, txid, assets_type
        });
        return;
      } catch (err) {
        await sleep('3s');
      }
    }
  }

  async checkWithdraw() {
    let page = 1;
    const pageSize = 50;
    while (true) {
      const res = await this.callApi('withdraw.query', {
        status: [2],
        assets_type: this.assets_type,
        page: page++,
        pageSize,
      });

      if (!(res && res.success)) {
        await sleep('5s');
        continue;
      }
      const data = res.data;
      const {count, list} = data;
      try {
        await this.handle(list);
      } catch (err) {
        console.log(err);
      }
      if (count < pageSize) {
        page = 1;
      }
      await sleep('5s');
    }
  }

  callApi(api, params, method='GET') {
    const host = process.env.apiHost || 'https://b.deals';
    let apiUrl;
    if (host.endsWith('/')) {
      apiUrl = host + 'deals/api.json';
    } else {
      apiUrl = host + '/deals/api.json';
    }

    const random = Math.random().toString();
    const data = {
      api,
      params,
      random,
    };
    const raw = rsa.encrypt(JSON.stringify(data), 'base64');
    return urllib.request(apiUrl, {
      timeout: '10s',
      method,
      data: {
        data: raw,
      },
      dataType: 'json',
    }).then(res => {
      if (res.status === 200) {
        return res.data;
      }
    });
  }

  get DataBase() {
    return DataBase;
  }

  async sleep(time) {
    await sleep(time);
  }

  formatBlock(block) {
    return {
      hash: block.hash,
      height: block.height || block.number,
      nextblockhash: block.nextblockhash,
      timestamp: block.timestamp || block.time,
    };
  }

  get minAmount() {
    return parseFloat(process.env.minAmount);
  }

  formatAmount(amount) {
    amount = amount.toString();
    const index = amount.indexOf('.');
    if (index > -1) {
      amount = amount.substring(0, index + 9);
    }
    return amount;
  }

  async beforeInit() {

  }

  handleConfig(config) {
    const json = JSON.parse(config);
    const data = json[this.assetsName.toLowerCase()] || [];
    this.withdrawAddress = {};
    for (let item of data) {
      const address = item.split(':')[0];
      this.withdrawAddress[address] = 1;
    }
  }

  handleTxOut({from, txid, block}) {

  }

  async init() {
    await this.beforeInit();
    let block;
    const progressFile = this.progressFile;
    const start = parseInt(process.env.start);
    if (start > 0) {
      this.lastBlock = {
        height: start - 5,
      };
    } else {
      if (fs.existsSync(progressFile)) {
        const content = fs.readFileSync(progressFile).toString();
        block = JSON.parse(content);
        this.lastBlock = block;
      } else {
        block = await this.getLatestBlock();
        if (block) {
          this.lastBlock = this.formatBlock(block);
          this.lastestBlock = this.lastBlock;
          this.onBlock(block);
        }
      }
    }

    this.debug('last block is %j', this.lastBlock);
    this.startSyncBlock();
    this.syncLatestBlock();
    this.startConfirmTransactions();
    this.syncPenddingTransactions();
  }

  async syncPenddingTransactions() {

  }

  async startConfirmTransactions() {
    const txdir = this.txdir;
    while (true) {
      await sleep(this.waitTime/3);
      const lastestBlock = this.lastestBlock;
      const list = fs.readdirSync(txdir);
      if (!lastestBlock || !(list && list.length)) {
        continue;
      }
      for (let name of list) {
        const txfile = path.join(txdir, name);
        const content = fs.readFileSync(txfile);
        let data = JSON.parse(content);
        if (data.height === 0) {
          if (this.getTransactionByHash) {
            try {
              const transaction = await this.getTransactionByHash(data.txid);
              if (transaction && transaction.height) {
                data = transaction;
                fs.writeFileSync(txfile, JSON.stringify(transaction));
              }
            } catch (err) {
              continue;
            }
          }
        }

        if (!data.height) {
          continue;
        }
        const confirms = lastestBlock.height - data.height;
        if (confirms >= this.confirmNumber - 1) {
          this.debug(`confirm transaction %j`, data);
          api.postConfirmedTransaction(data);
          fs.unlinkSync(txfile);
        }
      }
    }
  }

  notImplement() {
    throw new Error('not implemented!');
  }

  // 获取最新区块
  async getLatestBlock() {
    this.notImplement();
  }

  // 同步最新区块
  async syncLatestBlock() {
    while (true) {
      const lastestBlock = this.lastestBlock;
      const lastBlock = this.lastBlock;
      // 已经是最新区块，不需要做同步了
      const block = await this.getLatestBlock();
      if (lastestBlock && lastestBlock.height === block.height) {
        continue;
      }
      if (block) {
        this.onBlock(block, true);
        this.lastestBlock = this.formatBlock(block);
        if (lastestBlock && lastBlock && lastBlock.height === lastestBlock.height) {
          this.lastBlock = this.lastestBlock;
        }
      }
      await sleep(this.waitTime);
    }
  }

  // 同步区块
  async startSyncBlock() {
    while (true) {
      try {
        let time = Date.now();
        this.debug(`start sync block, last block is (%j)`, this.lastBlock);
        const lastestBlock = this.lastestBlock;
        // 已经是最新区块，不需要做同步了
        if (lastestBlock && lastestBlock.height === this.lastBlock.height) {
          await sleep('10s');
          continue;
        }
        const nextBlock = await this.getBlock(this.lastBlock);
        this.debug(`synced block ${(Date.now() - time)/1000}s`);
        if (nextBlock) {
          const block = this.formatBlock(nextBlock);
          this.onBlock(nextBlock);
          this.lastBlock = block;
          await sleep(100);
        } else {
          await sleep('10s');
        }
      } catch(err) {

      }
    }
  }

  get assetsName() {
    this.notImplement();
  }

  // 确认数
  get confirmNumber() {
    this.notImplement();
  }

  async getBlock(lastBlock) {
    this.notImplement();
  }

  async postTransaction(data) {
    data.assets_type = this.assets_type;
    const address = data.address;
    const has = await this.addressManager.has(address);
    if (!has) {
      return;
    }
    if (this.utxo !== false) {
      this.saveUtxo(data);
    }
    if (this.db && data.block && data.script) {
      await this.db.insert({
        txid: data.txid,
        block_hash: data.block,
        address: data.address,
        tx_index: data.tx_index,
        value: data.amount,
        script: data.script,
        status: 1,
      });
    }
    this.debug('post transaction', data);
    fs.writeFileSync(path.join(this.txdir, data.txid+'_'+data.address), JSON.stringify(data));
    api.postPendingTransaction(data);
  }

  handleTx(tx, height, blockHash) {
    this.notImplement();
  }

  getBlockTransactions(block) {
    return block.tx || block.transactions;
  }

  // 处理block
  onBlock(block, latest) {
    const formattedBlock = this.formatBlock(block);
    if (this.lastestBlock && this.lastestBlock.height < formattedBlock.height) {
      this.lastestBlock = formattedBlock;
    }

    if (!latest) {
      this.debug('save block %j', formattedBlock);
      fs.writeFileSync(this.progressFile, JSON.stringify(formattedBlock, null, 2));
    }

    const transactions = this.getBlockTransactions(block);
    if (!(transactions && transactions.length)) {
      return;
    }
    for (let tx of transactions) {
      this.handleTx(tx, formattedBlock.height, formattedBlock.hash);
    }
  }

  saveUtxo(tx) {
    const {txid, address, block, tx_index} = tx;
    if (!block) {
      return;
    }
    const addressFile = path.join(this.utxoAddressDir, address);

    let data = {};
    if (fs.existsSync(addressFile)) {
      const content = fs.readFileSync(addressFile);
      data = JSON.parse(content);
    }
    data[txid] = tx;
    fs.writeFileSync(addressFile, JSON.stringify(data));

    const id = txid + '_' + tx_index;
    const txfile = path.join(this.utxoTxDir, id);
    fs.writeFileSync(txfile, JSON.stringify(tx));
  }

  async updateUtxo(txid, vout, status) {
    const has = await this.hasUtxo(txid, vout);
    if (!has) {
      return;
    }
    await utxo.updateStatus(txid, vout, this.assets_type, status);
  }
}

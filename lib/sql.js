'use strict';
const sqlite3 = require('sqlite3');
const sleep = require('ko-sleep');
const path = require('path');
const mkdirp = require('mkdirp');
const fs = require('fs');
const os = require('os');
const ms = require('ms');
const debug = require('debug')('sync');
const sql = fs.readFileSync(path.join(__dirname, 'tx.sql')).toString();
const homedir = os.homedir();

class DataBase {
  constructor(name) {
    this.dbfile = path.join(__dirname, `${name}.db`);
    this.init();
  }

  exec(sql) {
    return new Promise((resolve, reject) => {
      this.db.exec(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  connectDataBase() {
    return new Promise((resolve, reject) => {
      const db = new sqlite3.Database(this.dbfile, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(db);
        }
      });
    });
  }

  query(sql) {
    return new Promise((resolve, reject) => {
      this.db.all(sql, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  run(sql) {
    return new Promise((resolve, reject) => {
      this.db.run(sql, (err, data) => {
        console.log(err, data);
        resolve(data);
      });
    });
  }

  async init() {
    this.db = await this.connectDataBase();
    await this.exec(sql);
    try {
      await this.createTable('CREATE INDEX index_address ON tx_in (address);');
    } catch (err) {

    }

    const ret = await this.query('select * from tx_in');
    console.log(ret);
    // await this.run(`
    //   insert into tx_in(tx_id, address, 'index', value, script, status)
    //   values('123', '456', 1, 1.2345, 'sss', 1)
    // `);

  }
}

new DataBase('btc');

'use strict';
const Redis = require('ioredis');
const path = require('path');
const fs = require('fs');
const os = require('os');
const sleep = require('ko-sleep');
const mkdirp = require('mkdirp');
const homedir = require('./homedir');
const byline = require('byline');
const watch = require('watch')
const env = process.env;

class AddressManager {
  constructor(name) {
    this.redis = new Redis({
      host: env.host || 'localhost',
      port: env.port || 6379,
    });
    this.isProcessing = false;
    this.addressDir = path.join(homedir, `.${name}.address`);
    mkdirp.sync(this.addressDir);
    this.process();

    watch.watchTree(this.addressDir,  (f, curr, prev) => {
      this.process();
    });
  }

  async process() {
    if (this.isProcessing) {
      return;
    }
    this.isProcessing = true;
    try {
      const list = fs.readdirSync(this.addressDir);
      if (!(list && list.length)) {
        return;
      }
      for (let name of list) {
        const file = path.join(this.addressDir, name);
        await this.handleFile(file);
      }
    } catch (err) {

    }
    this.isProcessing = false;
  }

  async handleFile(file) {
    const stream = byline(fs.createReadStream(file, { encoding: 'utf8' }));
    return new Promise(resolve => {
      stream.on('data', async (line) => {
        const address = line.toString().trim();
        if (!address) {
          return;
        }
        try {
          await this.add(address);
        } catch (err) {

        }
      }).on('end', () => {
        resolve();
      });
    })
  }

  async has(address) {
    const ret = await this.redis.get(`bdeal_address:${address}`);
    return ret && parseInt(ret) === 1;
  }

  async add(address) {
    await this.redis.set(`bdeal_address:${address}`, '1');
  }
}

module.exports =  AddressManager;

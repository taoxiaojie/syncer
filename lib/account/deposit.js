'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// 用户的充值记录
const depositSchema = new Schema({
  assets_type: String, // 资产类型
  txid: { type: String, index: true }, // 交易id
  block_hash: String, // 区块id
  block_height: Number, // 区块高度
  address: String, // 充值地址
  output_no: Number, // 序号
  value: String,// 充值金额
  status: Number, //状态1.进账中，2.已进账，3.出账中，4.已出账
  create_time: {type: Date, default: Date.now},
  withdraw_time: {type: Date}, // 归集时间
});

mongoose.model('deposits', depositSchema);
const depositModel = mongoose.model('deposits');

const deposit = new depositModel({
  assets_type: 1,
  txid: 1123,
  block_hash: '456',
  address: '242',
  output_no: 1,
  value: 34,
  status: 1,
  withdraw_time: Date.now(),
});

deposit.save(err => {
  console.log(err);
});

'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// 平台比特系热钱包管理
const hotWalletSchema = new Schema({
  assets_type: String, // 资产类型
  txid: { type: String, index: true }, // 交易id
  block_hash: String, // 区块id
  address: String, // 地址
  output_no: Number, // 序号
  value: String,
  assets_type: String,
  status: Number, //状态1.进账中  2.已进账 3.出账中 4.已出账
  create_time: {type: Date, default: Date.now},
});

mongoose.model('bit_hot_wallet', hotWalletSchema);
const withdrawModel = mongoose.model('bit_hot_wallet');

'use strict';
const request = require('request-promise-native');
const zlib = require('zlib');
const util = require('util')
const COS = require('cos-nodejs-sdk-v5');
const core = require('bitcore-lib');
const mongo = require('../lib/mongo');
const CryptoJS = require("crypto-js");
const aes = CryptoJS.AES;
const {Transaction, Script, Address} = core;
const co = require('co');
const cos = new COS({
  SecretId: 'AKID31thMqrLOGzlSxdYxHRl295tX0XypOsQ',
  SecretKey: 'c3JdpqiT4e5LUTJySnw1wjLm8OUMGHCz',
});

function encrypt(key, secret) {
  return aes.encrypt(key, secret).toString();
}

function decrypt(text, secret) {
  return aes.decrypt(text, secret).toString(CryptoJS.enc.Utf8);
}

const getBucket = util.promisify(cos.getBucket.bind(cos));
const getObject = util.promisify(cos.getObject.bind(cos));
const deleteObject = util.promisify(cos.deleteObject.bind(cos));

const btcMultisigAccount = mongo.getAccountModel('btc_multisig');
const Bucket = 'bdeals-transactions-1256497442';
const Region = 'ap-shanghai';

const API = 'https://insight.bitpay.com/api';

const fetchUnspents = (address) =>
  request(`${API}/addr/${address}/utxo/`).then(JSON.parse)

const broadcastTx = (txRaw) =>
  request.post(`${API}/tx/send`, {
    json: true,
    body: {
      rawtx: txRaw,
    },
  })

co(function*() {
  const res = yield getBucket({
    Bucket,
    Region,
    Prefix: '20180926/',
  });
  const contents = res.Contents.filter(item => item.Key && item.Key.endsWith('.tx'));
  for (let item of contents) {
    const params = {
      Bucket,
      Region,
      Key: item.Key,
    };

    const obj = yield getObject(params);
    if (obj.statusCode === 200 && obj.Body) {
      const tx = JSON.parse(zlib.inflateSync(obj.Body));
      const keys = [];
      for (let input of tx.inputs) {
        const address = Address.fromScript(new Script(input.output.script)).toString();
        const account = yield btcMultisigAccount.findOne({
          address,
        });
        const pk = decrypt(account.secret, '81b0427637aa55f7af85dd9fdd63fa2c');
        keys.push(pk);
      }
      yield deleteObject(params);
      const newTx = new Transaction(tx).sign(keys);
      console.log(yield broadcastTx(newTx.serialize()));
    }
  }
});

'use strict';
const BaseCollector = require('./base');
const BN = require('bignumber.js');
const mongo = require('../lib/mongo');
const md5 = require('md5');
const zlib = require('zlib');
const moment = require('moment');
const {PrivateKey, PublicKey, Address, crypto, HDPrivateKey, HDPublicKey} = require('bitcore-lib');
const scale = 1e8;
const baseFee = 0.0001; // 给一个地址转账的平均费用
const start = Math.pow(2, 30);
const assert = require('assert');
const COS = require('cos-nodejs-sdk-v5');
const addressMaker = require('../lib/address_maker');

module.exports = class SatoshiCollector extends BaseCollector {
  constructor(options = {}) {
    super(options);
    this.highWatermark = BN(options.highWatermark);
    const xpubkey = new HDPublicKey(options.xpubkey);
    this.xpubkey1 = xpubkey.derive(1024);
    this.xpubkey2 = xpubkey.derive(1025);
    this.depositModel = mongo.getBitDepositModel(this.tokenName);
    this.buffer = [];
    this.cos = new COS({
      SecretId: 'AKID31thMqrLOGzlSxdYxHRl295tX0XypOsQ',
      SecretKey: 'c3JdpqiT4e5LUTJySnw1wjLm8OUMGHCz',
    });
  }

  toSatoshi(value) {
    return BN(value).times(scale).toNumber();
  }

  getScript(address) {
    return new this.Script(new this.Address(address)).toHex()
  }

  async handleTransaction(tx) {
    const {value} = tx;
    if (value && value.toString() === '0.00000546') {
      return;
    }
    // 金额较大的充值立刻转入冷钱包
    if (1 || this.highWatermark.lte(value)) {
      const transaction = this.newTransaction();
      await this.addInput(transaction, tx);
      return await this.submitTransaction(transaction);
    }

    const has = this.buffer.some(item => {
      return item.txid === tx.txid;
    });

    if (has) {
      return;
    }
  }

  async submitTransaction(transaction) {
    const fee = transaction.inputs.length * baseFee * scale;
    let total = BN(0);
    const inputs = transaction.inputs;
    for (let item of inputs) {
      total = total.plus(item.output.satoshis);
    }
    const targetAddress = this.targetAddress;
    assert(targetAddress, 'targetAddress is required');
    transaction
      .to(targetAddress, total.minus(fee).toNumber())
      .fee(fee)
      .change(targetAddress)
      .sign(transaction.privateKeys);
    //  发送到腾讯云服务器等待另一端的签名
    const data = JSON.stringify(transaction.toObject());
    await this.sendToCloud(data);

    for (let input of inputs) {
      const {prevTxId, outputIndex} = input;
      const ret = await this.depositModel.updateOne({
        txid: prevTxId.toString('hex'),
        output_no: outputIndex,
      }, {
        status: 3,
      });
    }
  }

  sendToCloud(data) {
    data = zlib.deflateSync(data);
    const hash = md5(data).toLowerCase();
    const date = moment().format('YYYYMMDD');
    return new Promise((resolve, reject) => {
      this.cos.putObject({
          Bucket: 'bdeals-transactions-1256497442',
          Region: 'ap-shanghai',                       /* 必须 */
          Key : `${date}/${hash}.tx`,                           /* 必须 */
          Body: data,           /* 必须 */
          ContentLength: data.length,
      }, function(err, data) {
        if (err) {
          console.log(err);
          return reject(err);
        } else {
          console.log(data.Location);
          return resolve(data);
        }
      });
    });
  }

  newTransaction() {
    return new this.Transaction();
  }

  async getAccount(address) {
    const account = await super.getAccount(address);
    const number = account.number;

    return {
      ...account,
      publicKeys: [
        this.xpubkey1.derive(start + number).publicKey.toString(),
        this.xpubkey2.derive(start + number).publicKey.toString(),
      ]
    }
  }

  // 创建转账交易,用户充值的比特系钱包全部使用多重签名
  async addInput(transaction, input) {
    const account = await this.getAccount(input.address);
    const utxo = {
      txId: input.txid,
      outputIndex: input.output_no,
      address: input.address,
      script: this.getScript(input.address),
      satoshis: this.toSatoshi(input.value),
    };
    if (!transaction.privateKeys) {
      transaction.privateKeys = [];
    }
    transaction.privateKeys.push(account.secret);
    return transaction.from(utxo, account.publicKeys, 2);
  }
}

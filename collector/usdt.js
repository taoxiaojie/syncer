'use strict';
const BaseCollector = require('./base');
const {PrivateKey, PublicKey, Address, Script, HDPrivateKey, HDPublicKey} = require('bitcore-lib');

module.exports = class USDTCollector extends BaseCollector {
  constructor(options) {
    super(options);
  }

  async handleTransaction(tx) {
    const tx = new Transaction();

    const unspents = await fetchUnspents(address);

    const fundValue     = 546 // dust
    const feeValue      = 1000
    const totalUnspent  = unspents.reduce((summ, { satoshis }) => summ + satoshis, 0)
    const skipValue     = totalUnspent - fundValue - feeValue

    if (totalUnspent < feeValue + fundValue) {
      throw new Error(`Total less than fee: ${totalUnspent} < ${feeValue} + ${fundValue}`)
    }

    const utxoSpend = [];

    var privateKeys = [
      new PrivateKey('L31giBXrs6frYAUsu9Nwpr7xWZWi5wuPfF9RFMXGUv5BRCyGywfv'),
      new PrivateKey('KzDCGSBJz5Cofo3woabxHp1tRb3sRnwZEavGC64ELoTPHErAUdhK'),
    ];

    var publicKeys = privateKeys.map(PublicKey);

    var addr = new Address(publicKeys, 2);

    unspents.forEach(({ txid, vout, address, satoshis, ...rest }) => {
      utxoSpend.push({
        "txId" : txid,
        "outputIndex" : vout,
        "address" : address,
        "script" : new Script.fromAddress(new Address(address)),
        "satoshis" : satoshis,
      });
    });

    tx.from(utxoSpend, publicKeys, 2);

    const simple_send = [
      "6f6d6e69", // omni
      "0000",     // version
      "00000000001f", // 31 for Tether
      ('0000000000000000' + BigNumber(3).times(1e8).toString(16).toUpperCase()).slice(-16)
    ].join('')

    const data = Buffer.from(simple_send, "hex")

    tx.to(this.targetAddress, fundValue)
    tx.addData(data)
    tx.to('3EnibJZBpRUcydJmDb4TNxnjLxHuQZ4tH6', skipValue);

    tx.sign(privateKeys[0]);

    const obj = JSON.stringify(tx.toObject(), null, 2);

    // console.log(obj);
    const buf = new Buffer(obj);
    const zlib = require('zlib');

    let buf1 = buf.slice(0);

    for (var i = 0; i < 3; i++) {
      buf1 = zlib.deflateSync(buf1);
    }


    const newTx = new Transaction(JSON.parse(obj));

    newTx.sign(privateKeys[1]);
    return newTx
  }
}

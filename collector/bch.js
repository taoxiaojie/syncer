'use strict';
const SatoshiCollector = require('./satoshi');
const {PrivateKey, PublicKey, Address, Script, Transaction} = require('bitcoincashjs');

module.exports = class BCHCollector extends SatoshiCollector {
  constructor(options) {
    super(options);
  }

  get tokenName() {
    return 'bch';
  }

  get Script() {
    return Script;
  }

  get Transaction() {
    return Transaction;
  }

  get Address() {
    return Address;
  }
}

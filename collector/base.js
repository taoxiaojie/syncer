// 用户充值余额自动归集到冷钱包
'use strict';
const sleep = require('ko-sleep');
const Base = require('sdk-base');
const mongo = require('../lib/mongo');
const CryptoJS = require('crypto-js');
const assert = require('assert');
const aes = CryptoJS.AES;

module.exports = class BaseCollector extends Base {
  constructor(options={}) {
    super({
      initMethod: 'init',
    });
    assert(options.targetAddress, 'targetAddress is required');
    this.isCollecting = false;
    this.secret = options.secret;
    this.handler = options.handler;
    this.Account = mongo.getAccountModel(this.tokenName);
    this.targetAddress = options.targetAddress;
  }

  async getAccount(address) {
    const record = await this.Account.findOne({
      address,
    });
    const secret = aes.decrypt(record.secret, this.secret).toString(CryptoJS.enc.Utf8);
    return {
      number: record.number,
      address,
      secret,
    }
  }

  async init() {
    this.handler.on('transaction', (transaction) => {
      this.handleTransaction(transaction);
    });

    this.startCollect();
  }

  async startCollect() {
    while (true) {
      const depositModel = this.depositModel;
      const list = await depositModel.find({status: 2}).limit(10);
      for (let item of list) {
        this.handleTransaction(item);
      }
      await this.sleep('600s');
    }
  }

  async sleep(ms) {
    await sleep(ms);
  }
}

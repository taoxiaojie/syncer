'use strict';
const SatoshiCollector = require('./satoshi');
const {PrivateKey, PublicKey, Address, Script, HDPrivateKey, HDPublicKey, Transaction} = require('bitcore-lib');

module.exports = class BTCCollector extends SatoshiCollector {
  constructor(options) {
    super(options);
  }

  get tokenName() {
    return 'btc';
  }

  get PrivateKey() {
    return PrivateKey;
  }

  get PublicKey() {
    return PublicKey;
  }

  get Script() {
    return Script;
  }

  get Transaction() {
    return Transaction;
  }

  get Address() {
    return Address;
  }
}

'use strict';
const SatoshiCollector = require('./satoshi');
const {PrivateKey, PublicKey, Address, Script, HDPrivateKey, Transaction} = require('litecore-lib');

module.exports = class LTCCollector extends SatoshiCollector {
  constructor(options) {
    super(options);
  }

  get tokenName() {
    return 'ltc';
  }

  get Script() {
    return Script;
  }

  get Transaction() {
    return Transaction;
  }

  get Address() {
    return Address;
  }
}

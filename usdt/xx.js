'use strict';
const {Script, Address, PublicKey, PrivateKey, Transaction} = require('bitcore-lib');

const OmniClient = require('omnilayer-client');
const omnicore = new OmniClient({
  host: '192.168.0.2',
  port: 32775,
  username: 'bdeals',
  password: 'bdeals_bitcoin',
});
const scale = 100000000;

async function test() {
  try {
    const omnicoinId = 31; // usdt is 31

    const feerate = await omnicore.estimateFee(25);

    const utxos = [
      {
        "txId" : "1d939866145c1ce581746357a835ad0fe270760b92d44a596d1474a5b65132c4",
        "outputIndex" : 1,
        "address" : "1AsJkVwxkcYSYrAjGmx3ukc61DKEerQ9Ls",
        "script" : new Script.fromAddress(new Address('1AsJkVwxkcYSYrAjGmx3ukc61DKEerQ9Ls')),
        "satoshis" : 0.00000546 * scale,
      },
      {
        "txId" : "1e9bb90b36a096d2cdf50f9461a3bbb30edd78a85ce55dfdf981c9bb512b19af",
        "outputIndex" : 1,
        "address" : "1AsJkVwxkcYSYrAjGmx3ukc61DKEerQ9Ls",
        "script" : new Script.fromAddress(new Address('1AsJkVwxkcYSYrAjGmx3ukc61DKEerQ9Ls')),
        "satoshis" : 0.0001 * scale,
      }
    ];

    let total = 0;
    const fee = 0.00002 * scale;

    for (let item of utxos) {
      total += item.satoshis;
    }

    // console.log(utxos);
    const address = '19ik5tr3xQckVnPMu465KFc7SMjvuVUgRB';
    const privKey = 'c03564e90e8db69394e0e92dcbb5e8dd2ea89136b0f461f2bc7040be7d5f9ca1';
    var transaction = new Transaction().from(utxos).to(address, total - fee);

    console.log(new Script.fromAddress(new Address(address)).toString());
    const payload = await omnicore.omni_createpayload_simplesend(omnicoinId, "19.47200000");

    console.log('payload', payload);
    let rawtx = await omnicore.omni_createrawtx_opreturn(transaction.toString(), payload);

    console.log('rawtx', rawtx);
    rawtx = await omnicore.omni_createrawtx_reference(rawtx, address);

    console.log('xxx rawtx', rawtx);
    let tx = new Transaction();
    tx.fromString(rawtx);
    //tx.from(utxos);

    console.log(tx.toObject());

    tx.sign([privKey, privKey]);
    tx.serialize();

    // stringify transaction
    rawtx = tx.toString();

    console.log(rawtx);
    const decoded = await omnicore.decodeRawTransaction(rawtx);
    console.log({decoded}); // decoded will include txid

    //const txid = await omnicore.sendRawTransaction(rawtx);
    //console.log({txid});
  } catch(err) {
    console.log(err);
  }
}

test();

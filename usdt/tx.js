'use strict';
const urllib = require('urllib');
const bitcore = require('bitcore-lib');
const { Script, Address, PublicKey, PrivateKey, Transaction} = bitcore;
// const OmniClient = require('omnilayer-client');
const OmniClient = require('../lib/omniClient.js').OmniClient;
const Omni = require('../lib/omniClient.js').Omni;
const scale = 100000000;

const omnicore = new OmniClient({
  host: '192.168.0.2',
  port: 32775,
  user: 'bdeals',
  pass: 'bdeals_bitcoin',
});

const omnicoinId = 31; // usdt is 31

async function test() {


  // let total = 0;
  // const fee = 0.00002 * scale;
  //
  // for (let item of utxos) {
  //   total += item.satoshis;
  // }
  //
  // console.log(total);



  const balance = await omnicore.omniGetBalance('1EhiW934TztB72DwkLJ2imfHBceRKTPXCA', 31);

  const txs = await omnicore.omniListTransactions('1EhiW934TztB72DwkLJ2imfHBceRKTPXCA');
  console.log(txs);

  return console.log(balance);

  const privKey = 'c03564e90e8db69394e0e92dcbb5e8dd2ea89136b0f461f2bc7040be7d5f9ca1';
  const utxos = [
      {
        "txId" : "16825d26fac2e433d7b3aff8b900e384f6d58784606f6813bee96105754cfd47",
        "outputIndex" : 1,
        "address" : "15aiNs86ntF8pMpYW7SaTYDZSgpZDte5Jz",
        "script" : new Script.fromAddress(new Address('15aiNs86ntF8pMpYW7SaTYDZSgpZDte5Jz')),
        "satoshis" : 0.00000546 * scale,
      }
  ]

  var transaction = new bitcore.Transaction()
    .from(utxos);

    let payload1 = await omnicore.omniCreatepayloadSimplesend(omnicoinId, "1.0");
    console.log('payload1');
    // 000000000000001f0000000005f5e100
    // 000000000000001f0000000005f5e100
    console.log(payload1);

    let rawtx1 = transaction.toString();
    console.log('transaction');
    // 010000000147fd4c750561e9be13686f608487d5f684e300b9f8afb3d733e4c2fa265d82160100000000ffffffff0000000000
    // 010000000147fd4c750561e9be13686f608487d5f684e300b9f8afb3d733e4c2fa265d82160100000000ffffffff0000000000
    console.log(rawtx1);
    rawtx1 = await omnicore.omniCreaterawtxOpreturn(rawtx1, payload1);
    console.log('omniCreaterawtxOpreturn');
    // 010000000147fd4c750561e9be13686f608487d5f684e300b9f8afb3d733e4c2fa265d82160100000000ffffffff010000000000000000166a146f6d6e69000000000000001f0000000005f5e10000000000
    // 010000000147fd4c750561e9be13686f608487d5f684e300b9f8afb3d733e4c2fa265d82160100000000ffffffff010000000000000000166a146f6d6e69000000000000001f0000000005f5e10000000000
    console.log(rawtx1);

    rawtx1 = await omnicore.omniCreaterawtxReference(rawtx1, "128HHbgLdQ3tGKjUzLfWDCR6iCcHQVKfZu");
    console.log('omniCreaterawtxReference');
    // 010000000147fd4c750561e9be13686f608487d5f684e300b9f8afb3d733e4c2fa265d82160100000000ffffffff020000000000000000166a146f6d6e69000000000000001f0000000005f5e10022020000000000001976a9140c58a1ed498a17f37d989427edc1c7150aea988988ac00000000
    // 010000000147fd4c750561e9be13686f608487d5f684e300b9f8afb3d733e4c2fa265d82160100000000ffffffff020000000000000000166a146f6d6e69000000000000001f0000000005f5e10022020000000000001976a9140c58a1ed498a17f37d989427edc1c7150aea988988ac00000000

    omnicore.omniCreaterawtxChange(rawtx1, );

    var transaction = new bitcore.Transaction();
    transaction.fromString(rawtx1);

    console.log(transaction.toObject());

    transaction.to('19ik5tr3xQckVnPMu465KFc7SMjvuVUgRB', total - fee);
    transaction.sign(privKey);

    // let tx = new bitcore.Transaction();
    // tx.fromString(rawtx1);
    // tx.to('19ik5tr3xQckVnPMu465KFc7SMjvuVUgRB', total - fee);

    return console.log(transaction.toObject());

  // const api = 'https://chain.api.btc.com/v3/tools/tx-publish';
  // const res = await urllib.request(api, {
  //   method: 'post',
  //   dataType: 'json',
  //   data: {
  //     rawhex: rawtx,
  //   }
  // });
  // console.log(res.data);



  const decoded = await omnicore.decodeRawTransaction(rawtx);
  console.log({decoded}); // decoded will include txid

  const txid = await omnicore.sendRawTransaction(rawtx);
  console.log({txid});
}

test()

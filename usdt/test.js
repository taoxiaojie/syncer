const request = require('request-promise-native')
const bitcore = require('bitcore-lib');
const BigNumber = require('bignumber.js');
const { Script, Address, PublicKey, PrivateKey, Transaction} = bitcore;

const API = 'https://insight.bitpay.com/api';

const fetchUnspents = (address) =>
  request(`${API}/addr/${address}/utxo/`).then(JSON.parse)

const broadcastTx = (txRaw) =>
  request.post(`${API}/tx/send`, {
    json: true,
    body: {
      rawtx: txRaw,
    },
  })

const createSimpleSend = async (fetchUnspents, key_pair, recipient_address) => {

  const tx = new Transaction();

  const address = "3EnibJZBpRUcydJmDb4TNxnjLxHuQZ4tH6";
  const unspents = await fetchUnspents(address);

  const fundValue     = 546 // dust
  const feeValue      = 1000
  const totalUnspent  = unspents.reduce((summ, { satoshis }) => summ + satoshis, 0)
  const skipValue     = totalUnspent - fundValue - feeValue

  if (totalUnspent < feeValue + fundValue) {
    throw new Error(`Total less than fee: ${totalUnspent} < ${feeValue} + ${fundValue}`)
  }

  const utxoSpend = [];

  var privateKeys = [
    new PrivateKey('L31giBXrs6frYAUsu9Nwpr7xWZWi5wuPfF9RFMXGUv5BRCyGywfv'),
    new PrivateKey('KzDCGSBJz5Cofo3woabxHp1tRb3sRnwZEavGC64ELoTPHErAUdhK'),
  ];

  var publicKeys = privateKeys.map(PublicKey);

  var addr = new Address(publicKeys, 2);

  unspents.forEach(({ txid, vout, address, satoshis, ...rest }) => {
    utxoSpend.push({
      "txId" : txid,
      "outputIndex" : vout,
      "address" : address,
      "script" : new Script.fromAddress(new Address(address)),
      "satoshis" : satoshis,
    });
  });

  tx.from(utxoSpend, publicKeys, 2);

  const simple_send = [
    "6f6d6e69", // omni
    "0000",     // version
    "00000000001f", // 31 for Tether
    ('0000000000000000' + BigNumber(3).times(1e8).toString(16).toUpperCase()).slice(-16)
  ].join('')

  const data = Buffer.from(simple_send, "hex")

  console.log(skipValue);

  tx.to('1Cx84V3K56fPSBhhZPvV2G6EhJrp28eVXr', fundValue)
  tx.addData(data)
  tx.to('3EnibJZBpRUcydJmDb4TNxnjLxHuQZ4tH6', skipValue);

  tx.sign(privateKeys[0]);

  const obj = JSON.stringify(tx.toObject(), null, 2);

  // console.log(obj);
  const buf = new Buffer(obj);
  const zlib = require('zlib');

  let buf1 = buf.slice(0);

  for (var i = 0; i < 3; i++) {
    buf1 = zlib.deflateSync(buf1);
  }

  console.log(buf.length, buf1.length);

  console.log(buf1.toString('hex'));

  const newTx = new Transaction(JSON.parse(obj));
  //
  newTx.sign(privateKeys[1]);

  const obj1 = JSON.stringify(newTx.toObject(), null, 2);

  // console.log(obj1);
  return newTx
}

const omni_tx = createSimpleSend(fetchUnspents, null,  '1Cx84V3K56fPSBhhZPvV2G6EhJrp28eVXr')


omni_tx.then(tx => {
  // console.log(tx.serialize());
  // const txRaw = tx.buildIncomplete()
  //
  // console.log('hash', txRaw.getId())
  //
  // console.log(`"${txRaw.toHex()}"`)
  // console.log(txRaw)

  // broadcastTx(tx.serialize())
})

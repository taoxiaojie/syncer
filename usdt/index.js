'use strict';
const urllib = require('urllib');
const BaseSyncer = require('../lib/bit_syncer');
const debug = require('debug')('btc');
const Client = require('bitcoin-core');
const OmniClient = require('../lib/omniClient.js').OmniClient;
const blockchain = require('blockchain.info');
const block_port = process.env.block_port || 8332;

module.exports = class Syncer extends BaseSyncer {
  constructor(options={}) {
    options.addressName = 'btc';
    super(options);
    this.baseUrl = `http://${this.host}:${block_port}/rest/`;

    this.client = new OmniClient({
      host: this.host,
      port: block_port,
      user: 'bdeals',
      pass: 'bdeals_bitcoin',
    });
  }

  // 同步未确认的区块
  async syncPenddingTransactions() {
    while (true) {
      await this.sleep('5s');
      try {
        const list = await this.client.omniListPendingTransactions();
        for (let item of list) {
          const propertyid = parseInt(item.propertyid);
          if (propertyid === 31 && item.valid) {
            await this.postTransaction({
              txid: tx.txid,
              address: tx.referenceaddress,
              amount: tx.amount,
              block: tx.blockhash,
            });
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  }

  get assetsName() {
    return 'usdt';
  }

  get assets_type() {
    return 5;
  }

  get confirmNumber() {
    return parseInt(process.env.confirmNumber) || 2;
  }

  get minAmount() {
    return 1;
  }

  async getBlock(lastBlock) {
    const height = lastBlock.height;
    return await this.getBlockByHeight(height);
  }

  // 根据交易id获取交易详细
  async getTx(txid) {
    return await this.client.omniGetTransaction(txid);
  }

  async getBlockByHeight(height) {
    const txs = await this.client.omniListBlockTransactions(height);
    for(let id of txs) {
      const tx = await this.getTx(id);
      if (parseInt(tx.propertyid) == 31 && tx.valid) {
        await this.postTransaction({
          height: tx.block,
          txid: tx.txid,
          address: tx.referenceaddress,
          amount: tx.amount,
          block: tx.blockhash,
        });
      }
    }
  }

  async postTransaction(data) {
    super.postTransaction(data);
  }

  async isSpent(txid, output_no) {
    const res = await urllib.request(`https://chain.so/api/v2/is_tx_spent/BTC/${txid}/${output_no}`);
    const data = res && res.data;
    if (data && data.status === 'success') {
      return data.data.is_spent;
    }
  }
}
